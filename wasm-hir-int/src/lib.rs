extern crate conv;
extern crate wasm_hir as hir;

use std::collections::HashMap;
use std::mem;
use conv::prelude::*;
use hir::TableId;
use hir::Value;

pub type Error = Box<std::error::Error>;

pub trait ImportModule {
    fn call(&self, sig: &hir::ImportSig, params: &[Value]) -> Result<Option<Value>, Error>;
}

pub struct State<'i: 'a, 'a> {
    pub import_modules: HashMap<String, &'a ImportModule>,
    pub mem: Vec<u8>,
    pub module: &'a hir::Module<'i>,
}

impl<'i: 'a, 'a> State<'i, 'a> {
    pub fn new(module: &'a hir::Module<'i>) -> Self {
        let mut mem = vec![0; module.memory.min.value_into().unwrap()];
        State::reset_memory(&mut mem, &module);
        State {
            import_modules: HashMap::new(),
            mem: mem,
            module: module,
        }
    }

    pub fn add_import(&mut self, name: String, import: &'a ImportModule) -> Result<(), Error> {
        if self.import_modules.contains_key(&name) {
            return Err(Error::from(format!("import module already exists under name {:?}", name)));
        }

        self.import_modules.insert(name, import);
        Ok(())
    }

    pub fn coerce_params(&self, func_id: hir::FuncId, params: &mut [Value]) -> Result<(), Error> {
        let param_tys = self.module.funcs[func_id.into_usize()].params.iter()
            .map(|param| param.type_);
        if param_tys.len() != params.len() {
            return Err(Error::from(format!("wrong number of parameters \
                (expected {}, got {})", param_tys.len(), params.len())));
        }
        for (p, p_ty) in params.iter_mut().zip(param_tys) {
            *p = p.coerce_to(p_ty);
        }
        Ok(())
    }

    pub fn direct_invoke_func(&mut self, func_id: hir::FuncId, params: &[Value]) -> Result<Option<Value>, Error> {
        let func = &self.module.funcs[func_id.into_usize()];
        let mut expr_state = EvalState {
            funcs: &self.module.funcs,
            imports: &self.module.imports,
            import_modules: &self.import_modules,
            mem: &mut self.mem,
        };
        expr_state.invoke_func(func, params)
    }

    pub fn invoke_func(&mut self, func: &str, params: &[Value]) -> Result<Option<Value>, Error> {
        let func_id = try!(self.lookup_export(func)
            .ok_or_else(|| Error::from(
                format!("could not find exported function {:?}", func))));
        self.direct_invoke_func(func_id, params)
    }

    pub fn invoke_func_coerce(&mut self, func: &str, params: &[Value]) -> Result<Option<Value>, Error> {
        let func_id = try!(self.lookup_export(func)
            .ok_or_else(|| Error::from(
                format!("could not find exported function {:?}", func))));
        let params: Vec<_> = {
            let mut params = params.to_vec();
            try!(self.coerce_params(func_id, &mut params));
            params
        };
        self.direct_invoke_func(func_id, &params)
    }

    pub fn lookup_export(&self, func: &str) -> Option<hir::FuncId> {
        self.module.exports.iter()
            .filter_map(|export| {
                if &*export.name == func {
                    Some(export.func)
                } else {
                    None
                }
            })
            .next()
    }

    pub fn reset(&mut self) {
        State::reset_memory(&mut self.mem, &self.module);
    }

    pub fn run_asserts(&mut self, asserts: &[hir::Assert<'i>]) -> Result<(), Vec<Error>> {
        let mut errs = vec![];

        for (i, assert) in asserts.iter().enumerate() {
            use hir::Assert;
            match *assert {
                Assert::Return(ref name, ref params, cmp_eq) => {
                    match self.invoke_func(name, params) {
                        Ok(res) if res == cmp_eq => { /* Do nothing. */ },
                        Ok(res) => errs.push(Error::from(format!(
                            "assert {} failed: expected {:?}, got {:?}",
                            i, cmp_eq, res))),
                        Err(err) => errs.push(Error::from(format!(
                            "assert {} failed: {}", i, err))),
                    } 
                },
                Assert::Trap(ref name, ref params, ref trap_msg) => {
                    match self.invoke_func(name, params) {
                        Ok(res) => errs.push(Error::from(format!(
                            "assert {} failed: expected trap {:?}, got {:?}",
                            i, trap_msg, res))),
                        Err(err) => {
                            let err_msg = format!("{}", err);
                            if !err_msg.contains(&trap_msg[..]) {
                                errs.push(Error::from(format!(
                                    "assert {} failed: expected trap {:?}, \
                                        got {:?}",
                                    i, trap_msg, err_msg)));
                            }
                        },
                    }
                },
            }
        }

        if errs.len() == 0 {
            Ok(())
        } else {
            Err(errs)
        }
    }

    fn reset_memory(mem: &mut Vec<u8>, module: &hir::Module<'i>) {
        assert!(module.memory.min <= module.memory.max);
        // TODO: Initialise disjoint regions
        for b in mem.iter_mut() { *b = 0 }
        for segment in &module.memory.segments {
            let src = &segment.data[..];
            let off = segment.offset as usize;
            let end = off + src.len();
            let dst = &mut mem[off..end];
            let len = ::std::cmp::min(src.len(), dst.len());
            for i in 0..len {
                dst[i] = src[i];
            }
        }
        mem.truncate(module.memory.min.value_into().unwrap());
    }
}

enum EvalResult {
    Branch(hir::BranchDepth, Option<Value>),
    Return(Option<Value>),
    Value(Value),
    Void,
}

macro_rules! try_eval {
    ($e:expr => !Void) => {
        match try!($e) {
            EvalResult::Value(v) => v,
            EvalResult::Void => panic!("expected value, got void"),

            e @ EvalResult::Branch(..)
            | e @ EvalResult::Return(..)
            => return Ok(e),
        }
    };

    ($e:expr => ?Void) => {
        match try!($e) {
            EvalResult::Value(v) => Some(v),
            EvalResult::Void => None,

            e @ EvalResult::Branch(..)
            | e @ EvalResult::Return(..)
            => return Ok(e),
        }
    };

    ($e:expr => $ty:ty) => {
        match try!($e) {
            EvalResult::Value(v) => match <$ty>::from_value(v) {
                Some(v) => v,
                None => panic!("expected {}, got {:?}", stringify!($ty), v),
            },
            EvalResult::Void => panic!("expected {}, got void", stringify!($ty)),

            e @ EvalResult::Branch(..)
            | e @ EvalResult::Return(..)
            => return Ok(e),
        }
    };
}

struct EvalState<'i: 'a, 'a> {
    funcs: &'a [hir::Func<'i>],
    imports: &'a [hir::Import<'i>],
    import_modules: &'a HashMap<String, &'a ImportModule>,
    mem: &'a mut Vec<u8>,
}

impl<'i: 'a, 'a> EvalState<'i, 'a> {
    pub fn invoke_func(&mut self, func: &hir::Func<'i>, params: &[Value]) -> Result<Option<Value>, Error> {
        let mut locals = params.iter().cloned()
            .chain(func.locals.iter()
                .map(|b| b.type_.initializer()))
            .collect::<Vec<_>>();

        let mut frame = EvalFrame {
            state: self,
            locals: &mut locals,
        };

        for expr in &func.body[..func.body.len()-1] {
            try!(frame.eval_expr(expr));
        }

        let result = try!(frame.eval_expr(func.body.last().unwrap()));
        match result {
            EvalResult::Branch(..) => panic!("attempted to branch out of function"),
            EvalResult::Return(v) => Ok(v),
            EvalResult::Value(v) => Ok(Some(v)),
            EvalResult::Void => Ok(None),
        }
    }
}

struct EvalFrame<'i: 'a, 'a: 'b, 'b> {
    state: &'b mut EvalState<'i, 'a>,
    locals: &'b mut [Value],
}

impl<'i: 'a, 'a: 'b, 'b> EvalFrame<'i, 'a, 'b> {
    pub fn eval_expr(&mut self, expr: &hir::Expr) -> Result<EvalResult, Error> {
        use hir::ExprNode as EN;
        match expr.node {
            EN::Binary(ref op, ref lhs, ref rhs) => {
                use hir::Binary as Bin;
                let lhs = try_eval!(self.eval_expr(lhs) => !Void);
                let rhs = try_eval!(self.eval_expr(rhs) => !Void);

                macro_rules! bin {
                    ($lhs_ty:ident, $op:expr, $rhs_ty:ident) => {
                        match ($lhs_ty::from_value(lhs), $rhs_ty::from_value(rhs)) {
                            (Some(lhs), Some(rhs)) => $op(lhs, rhs).into(),
                            (None, _) => panic!("failed to convert lhs ({:?}) to {}", lhs, stringify!($lhs_ty)),
                            (_, None) => panic!("failed to convert rhs ({:?}) to {}", rhs, stringify!($rhs_ty)),
                        }
                    }
                }

                Ok(EvalResult::Value(match *op {
                    Bin::I32Add => bin!(i32, i32::wrapping_add, i32),
                    Bin::I32And => bin!(i32, |a, b| a & b, i32),
                    Bin::I32Mul => bin!(i32, i32::wrapping_mul, i32),
                    Bin::I32Sub => bin!(i32, i32::wrapping_sub, i32),

                    Bin::I64Add => bin!(i64, i64::wrapping_add, i64),
                    Bin::I64And => bin!(i64, |a, b| a & b, i64),
                    Bin::I64Mul => bin!(i64, i64::wrapping_mul, i64),
                    Bin::I64Sub => bin!(i64, i64::wrapping_sub, i64),
                }))
            },

            EN::Block(ref exprs) => {
                use self::EvalResult as ER;
                let mut last_result = ER::Void;

                for expr in exprs {
                    match try!(self.eval_expr(expr)) {
                        ER::Branch(0, v) => {
                            return Ok(v.map(ER::Value).unwrap_or(ER::Void));
                        },
                        ER::Branch(d, v) => {
                            return Ok(ER::Branch(d-1, v));
                        },

                        r @ ER::Return(..) => return Ok(r),
                        v @ ER::Value(..) => last_result = v,
                        v @ ER::Void => last_result = v,
                    }
                }

                Ok(last_result)
            },

            EN::Br(branch, ref m_expr) => {
                let m_expr = match *m_expr {
                    Some(ref expr) => try_eval!(self.eval_expr(expr) => ?Void),
                    None => None,
                };

                Ok(EvalResult::Branch(branch, m_expr))
            },

            EN::Call(ref func_id, ref params) => {
                let func = &self.state.funcs[func_id.into_usize()];
                let mut param_values: Vec<Value> = Vec::with_capacity(params.len());
                for param in params {
                    param_values.push(try_eval!(self.eval_expr(param) => !Void));
                }

                match try!(self.state.invoke_func(func, &param_values)) {
                    Some(v) => Ok(EvalResult::Value(v)),
                    None => Ok(EvalResult::Void),
                }
            },

            EN::CallImport(import_id, ref params) => {
                let import = &self.state.imports[import_id.into_usize()];
                let mut param_values: Vec<Value> = Vec::with_capacity(params.len());
                for param in params {
                    param_values.push(try_eval!(self.eval_expr(param) => !Void));
                }

                let import_module = match self.state.import_modules.get(&import.module[..]) {
                    Some(v) => v,
                    None => return Err(Error::from(format!("could not locate import module {:?}", &import.module[..]))),
                };
                let import_sig = &import.sig;

                match import_module.call(import_sig, &param_values) {
                    Ok(Some(v)) => Ok(EvalResult::Value(v)),
                    Ok(None) => Ok(EvalResult::Void),
                    Err(err) => Err(err),
                }
            },

            EN::Compare(op, ref lhs, ref rhs) => {
                use hir::Compare as Cmp;
                let lhs = try_eval!(self.eval_expr(lhs) => !Void);
                let rhs = try_eval!(self.eval_expr(rhs) => !Void);
                Ok(EvalResult::Value(match op {
                    Cmp::F32Eq | Cmp::F64Eq | Cmp::I32Eq | Cmp::I64Eq
                    => (lhs == rhs).into(),
                    Cmp::F32Ne | Cmp::F64Ne | Cmp::I32Ne | Cmp::I64Ne
                    => (lhs != rhs).into(),
                }))
            },

            EN::Const(val) => Ok(EvalResult::Value(val)),

            EN::If(ref cond, ref then, ref m_else) => {
                let cond = try_eval!(self.eval_expr(cond) => i32);
                if let Some(ref else_) = *m_else {
                    if cond != 0 {
                        self.eval_expr(then)
                    } else {
                        self.eval_expr(else_)
                    }
                } else {
                    if cond != 0 {
                        let _ = try_eval!(self.eval_expr(then) => ?Void);
                    }
                    Ok(EvalResult::Void)
                }
            }

            EN::GetLocal(local_id) => {
                Ok(EvalResult::Value(self.locals[local_id.into_usize()]))
            },

            EN::Load(kind, offset, align, ref addr) => {
                use hir::Load as L;
                let addr = try_eval!(self.eval_expr(addr) => usize);
                let addr = try!(addr.resolve_address(offset, align));

                let mem = &mut self.state.mem;
                if addr >= mem.len() {
                    return Err(Error::from(format!(
                        "out of bounds memory access (load {:?})", addr)));
                }
                let mem = &mem[addr..];

                Ok(EvalResult::Value(match kind {
                    L::F32 => try!(f32::load_from(mem)).into(),
                    L::F64 => try!(f64::load_from(mem)).into(),
                    L::I32 => try!(i32::load_from(mem)).into(),
                    L::I32FromI8 => (try!(i8::load_from(mem)) as i32).into(),
                    L::I32FromU8 => (try!(u8::load_from(mem)) as i32).into(),
                    L::I32FromI16 => (try!(i16::load_from(mem)) as i32).into(),
                    L::I32FromU16 => (try!(u16::load_from(mem)) as i32).into(),
                    L::I64 => try!(i64::load_from(mem)).into(),
                    L::I64FromI8 => (try!(i8::load_from(mem)) as i64).into(),
                    L::I64FromU8 => (try!(u8::load_from(mem)) as i64).into(),
                    L::I64FromI16 => (try!(i16::load_from(mem)) as i64).into(),
                    L::I64FromU16 => (try!(u16::load_from(mem)) as i64).into(),
                    L::I64FromI32 => (try!(i32::load_from(mem)) as i64).into(),
                    L::I64FromU32 => (try!(u32::load_from(mem)) as i64).into(),
                }))
            },

            EN::Loop(ref exprs) => {
                use self::EvalResult as ER;

                'outer: loop {
                    'inner: for expr in exprs {
                        match try!(self.eval_expr(expr)) {
                            ER::Branch(0, None) => {
                                continue 'outer;
                            },
                            ER::Branch(0, _) => {
                                panic!("got non-void branch back to loop");
                            },
                            ER::Branch(d, v) => {
                                return Ok(ER::Branch(d-1, v));
                            },

                            r @ ER::Return(..) => return Ok(r),
                            ER::Value(..) => (),
                            ER::Void => (),
                        }
                    }
                }
            },

            EN::Return(ref m_expr) => {
                let m_expr = match *m_expr {
                    Some(ref expr) => try_eval!(self.eval_expr(expr) => ?Void),
                    None => None,
                };

                Ok(EvalResult::Return(m_expr))
            },

            EN::SetLocal(local_id, ref expr) => {
                let expr = try_eval!(self.eval_expr(expr) => !Void);
                self.locals[local_id.into_usize()] = expr;
                Ok(EvalResult::Value(expr))
            },

            EN::Store(kind, offset, align, ref addr, ref value) => {
                use hir::Store as S;
                let addr = try_eval!(self.eval_expr(addr) => usize);
                let addr = try!(addr.resolve_address(offset, align));
                let value = try_eval!(self.eval_expr(value) => !Void);

                let mem = &mut self.state.mem;
                if addr >= mem.len() {
                    return Err(Error::from(format!(
                        "out of bounds memory access (store {:?})", addr)));
                }
                let mem = &mut mem[addr..];

                macro_rules! st {
                    ($value_ty:ident) => {
                        match $value_ty::from_value(value) {
                            Some(value) => try!(value.store_to(mem)),
                            None => panic!("failed to convert value ({:?}) to {}", value, stringify!($value_ty)),
                        }
                    };
                    ($value_ty:ident, $conv_ty:ident) => {
                        match $value_ty::from_value(value) {
                            Some(value) => try!((value as $conv_ty).store_to(mem)),
                            None => panic!("failed to convert value ({:?}) to {}", value, stringify!($value_ty)),
                        }
                    };
                }

                match kind {
                    S::F32 => st!(f32),
                    S::F64 => st!(f64),
                    S::I32 => st!(i32),
                    S::I32AsB8 => st!(i32, i8),
                    S::I32AsB16 => st!(i32, i16),
                    S::I64 => st!(i64),
                    S::I64AsB8 => st!(i64, i8),
                    S::I64AsB16 => st!(i64, i16),
                    S::I64AsB32 => st!(i64, i32),
                }

                Ok(EvalResult::Value(value))
            },

            EN::Unary(op, ref arg) => {
                use hir::Unary as Una;
                let arg = try_eval!(self.eval_expr(arg) => !Void);

                macro_rules! una {
                    ($arg_ty:ident, $op:expr) => {
                        match $arg_ty::from_value(arg) {
                            Some(arg) => $op(arg).into(),
                            None => panic!("failed to convert arg ({:?}) to {}", arg, stringify!($arg_ty)),
                        }
                    };
                }

                Ok(EvalResult::Value(match op {
                    Una::F64FromI32 => una!(i32, f64::from),
                    Una::F64FromI64Bits => una!(i64, i64::bit_cast_to::<f64>),
                }))
            },
        }
    }
}

trait HirType: Sized {
    fn from_value(v: Value) -> Option<Self>;
    fn hir_type() -> hir::ValueType;
}

impl HirType for f32 {
    fn from_value(v: Value) -> Option<Self> {
        match v {
            Value::F32(v) => Some(v),
            _ => None,
        }
    }

    fn hir_type() -> hir::ValueType {
        hir::ValueType::F32
    }
}

impl HirType for f64 {
    fn from_value(v: Value) -> Option<Self> {
        match v {
            Value::F64(v) => Some(v),
            _ => None,
        }
    }

    fn hir_type() -> hir::ValueType {
        hir::ValueType::F64
    }
}

impl HirType for i32 {
    fn from_value(v: Value) -> Option<Self> {
        match v {
            Value::I32(v) => Some(v),
            _ => None,
        }
    }

    fn hir_type() -> hir::ValueType {
        hir::ValueType::I32
    }
}

impl HirType for i64 {
    fn from_value(v: Value) -> Option<Self> {
        match v {
            Value::I64(v) => Some(v),
            _ => None,
        }
    }

    fn hir_type() -> hir::ValueType {
        hir::ValueType::I64
    }
}

impl HirType for usize {
    fn from_value(v: Value) -> Option<Self> {
        match v {
            Value::I32(v) => Some((v as u32) as usize),
            _ => None,
        }
    }

    fn hir_type() -> hir::ValueType {
        hir::Arch::Wasm32.address_value_type()
    }
}

trait AddressExt: Sized {
    fn resolve_address(self, offset: hir::Offset, align: hir::Align) -> Result<Self, Error>;
}

impl AddressExt for usize {
    fn resolve_address(self, offset: hir::Offset, align: hir::Align) -> Result<Self, Error> {
        assert!(align.count_ones() == 1);
        let addr = self as u64;

        let addr = if offset == ::std::i64::MIN {
            return Err(Error::from(format!("offset too large ({})", offset)));
        } else if offset >= 0 {
            let offset = offset as u64;
            try!(addr.checked_add(offset)
                .ok_or_else(|| format!("offset too large ({} + {})", addr, offset)))
        } else {
            let offset = (-offset) as u64;
            try!(addr.checked_sub(offset)
                .ok_or_else(|| format!("offset too large ({} - {})", addr, offset)))
        };

        let addr = addr & !(align as u64 - 1);
        Ok(try!(
            addr.value_into().map_err(|_| format!(
                "offset too large for architecture ({})", addr))
        ))
    }
}

trait BitCast<Target>: Sized {
    fn bit_cast(self) -> Target;
}

impl BitCast<f64> for i64 {
    fn bit_cast(self) -> f64 {
        unsafe { mem::transmute(self) }
    }
}

trait BitCastTo: Sized {
    fn bit_cast_to<Target>(self) -> Target where Self: BitCast<Target> {
        self.bit_cast()
    }
}

impl BitCastTo for i64 {}

trait LoadExt: Sized {
    fn load_from(mem: &[u8]) -> Result<Self, Error>;
}

impl LoadExt for f32 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for f64 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for i8 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for u8 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for i16 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for u16 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for i32 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for u32 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for i64 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }
impl LoadExt for u64 { fn load_from(mem: &[u8]) -> Result<Self, Error> { mem_load(mem) } }

fn mem_load<T: Copy>(mem: &[u8]) -> Result<T, Error> {
    unsafe {
        let size = mem::size_of::<T>();
        if mem.len() < size {
            return Err(Error::from(format!(
                "out of bounds memory access (load of size {})", size)));
        }
        Ok(*(mem.as_ptr() as *const T))
    }
}

trait StoreExt: Sized {
    fn store_to(self, mem: &mut [u8]) -> Result<(), Error>;
}

impl StoreExt for f32 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }
impl StoreExt for f64 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }
impl StoreExt for i8 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }
impl StoreExt for i16 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }
impl StoreExt for i32 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }
impl StoreExt for i64 { fn store_to(self, mem: &mut [u8]) -> Result<(), Error> { mem_store(mem, self) } }

fn mem_store<T: Copy>(mem: &mut [u8], value: T) -> Result<(), Error> {
    unsafe {
        let size = mem::size_of::<T>();
        if mem.len() < size {
            return Err(Error::from(format!(
                "out of bounds memory access (store of size {})", size)));
        }
        *(mem.as_mut_ptr() as *mut T) = value;
        Ok(())
    }
}

trait ROValueExt: Sized {
    fn require_value(self) -> Result<Value, Error>;
}

impl ROValueExt for Result<Option<Value>, Error> {
    fn require_value(self) -> Result<Value, Error> {
        match self {
            Ok(Some(v)) => Ok(v),
            Ok(None) => Err("expected value".into()),
            Err(err) => Err(err),
        }
    }
}
