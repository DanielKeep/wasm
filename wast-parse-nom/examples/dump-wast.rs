extern crate nom;
extern crate wast_parse;

const SOURCE: &'static [u8] = br##"
(module
  (export "add" $add)
)
"##;
/*
  ;; (memory 256 256)
  ;; (func $add (param $x i32) (param $y i32) (result i32)
  ;;   (i32.add
  ;;     (get_local $x)
  ;;     (get_local $y)
  ;;   )
  ;; )
*/

fn main() {
    match wast_parse::module(SOURCE) {
        Ok(v) => println!("{:#?}", v),
        Err(nom::Err::Code(kind)) => println!("error: {:?}", kind),
        Err(nom::Err::Node(kind, err)) => println!("error: {:?}, {}", kind, err),
        Err(nom::Err::Position(kind, pos)) => println!("error: {:?}, at: {:?}", kind, std::str::from_utf8(pos).unwrap()),
        Err(nom::Err::NodePosition(kind, pos, err)) => println!("error: {:?}, {}, at: {:?}", kind, err, std::str::from_utf8(pos).unwrap()),
    }
}
