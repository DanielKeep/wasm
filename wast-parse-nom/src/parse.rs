#![allow(dead_code)]

use std::borrow::Cow;
use nom::{self, IResult, hex_digit, multispace, not_line_ending};

#[derive(Debug)]
pub struct Module {
    entries: Vec<ModuleEntry>,
}

#[derive(Debug)]
pub enum ModuleEntry {
    Type,
    Func,
    Import,
    Export,
    Table,
    Memory,
    Start,
}

pub fn module_(bs: &[u8]) -> Result<Module, nom::Err<&[u8]>> {
    match module(bs) {
        IResult::Done(_, v) => Ok(v),
        IResult::Error(err) => Err(err),
        IResult::Incomplete(_) => Err(nom::Err::Code(nom::ErrorKind::Complete)),
    }
}

named!(
    module <Module>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("module") ~ white?
        ~ entries: many0!(module_entry) ~ white?
        ~ tag!(")") ~ white?
        ,
        || Module {
            entries: entries,
        }
    )
);

named!(
    module_entry <ModuleEntry>,
    chain!(
        white?
        ~ r: alt!(
            module_type
            | func
            | import
            | export
            | table
            | memory
            | start
        ) ~ white?
        ,
        || {
            println!("Got {:?}", r);
            r
        }
    )
);

named!(
    module_type <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("type") ~ white?
        // ~ name_: name? ~ white?
        // ~ tag!("(") ~ white?
        // ~ tag!("func") ~ white?
        // ~ params: many0!(param) ~ white?
        // ~ result_: result? ~ white?
        // ~ tag!(")") ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Type
    )
);

named!(
    func <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("func") ~ white?
        // ~ name_: name? ~ white?
        // ~ type__: type_? ~ white?
        // ~ params: many0!(param) ~ white?
        // ~ result_: result? ~ white?
        // ~ locals: many0!(local) ~ white?
        // ~ exprs: many0!(expr) ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Func
    )
);

named!(
    import <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("import") ~ white?
        // ~ name_: name? ~ white?
        // ~ str0: string ~ white?
        // ~ str1: string ~ white?
        // ~ params: chain!(
        //     tag!("(") ~ white?
        //     ~ tag!("param") ~ white?
        //     ~ types: many0!(type_) ~ white?
        //     ~ tag!(")") ~ white?
        //     ,
        //     || types
        // )? ~ white?
        // ~ result: chain!(
        //     tag!("(") ~ white?
        //     ~ tag!("result") ~ white?
        //     ~ type__: type_ ~ white?
        //     ~ tag!(")") ~ white?
        //     ,
        //     || type__
        // )? ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Import
    )
);

named!(
    export <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("export") ~ white?
        ~ _str0: string ~ white?
        // ~ export: alt!(
        //     map!(var, |v| "TODO")
        //     | map!(memory, |v| "TODO")
        // ) ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Export
    )
);

named!(
    table <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("table") ~ white?
        // ~ vars: many0!(var) ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Table
    )
);

named!(
    memory <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("memory") ~ white?
        ~ int0: int ~ white?
        ~ int1: int? ~ white?
        // ~ segments: many0!(segment) ~ white?
        ~ tag!(")") ~ white?
        ,
        || {
            println!("Got (memory {:?} {:?})", int0, int1);
            ModuleEntry::Memory
        }
    )
);

named!(
    start <ModuleEntry>,
    chain!(
        white?
        ~ tag!("(") ~ white?
        ~ tag!("start") ~ white?
        // ~ var_: var ~ white?
        ~ tag!(")") ~ white?
        ,
        || ModuleEntry::Start
    )
);

pub enum Var {
    Int,
    Name,
}

named!(
    var <Var>,
    alt!(
        map!(int, |_| Var::Int)
        | chain!(
            tag!("$")
            ~ _v: name
            ,
            || Var::Name
        )
    )
);

named!(
    int <u64>,
    map!(
        take_while1!(is_digit),
        |bs| unsafe {
            let s = ::std::str::from_utf8_unchecked(bs);
            s.parse().unwrap()
        }
    )
);

named!(
    string < Cow<str> >,
    map!(
        delimited!(
            tag!("\""),
            recognize!(
                many0!(
                    alt!(
                        tag!("\\n")
                        | tag!("\\t")
                        | tag!("\\\\")
                        | tag!("\\'")
                        | tag!("\\\"")
                        | recognize!(
                            chain!(
                                tag!("\\")
                                ~ hex_digit
                                ~ hex_digit
                                ,
                                || ()
                            )
                        )
                        | is_not!("\\\"")
                    )
                )
            ),
            tag!("\"")
        ),
        |bs| unsafe { ::std::str::from_utf8_unchecked(bs).into() }
    )
);

named!(
    name <&str>,
    map!(
        take_while1!(is_letter),
        |bs| unsafe { ::std::str::from_utf8_unchecked(bs) }
    )
);

fn is_digit(b: u8) -> bool {
    match b as char {
        '0'...'9' => true,
        _ => false
    }
}

fn is_letter(b: u8) -> bool {
    match b as char {
        'A'...'Z' | 'a'...'z' | '0'...'9'
        | '_' | '.' | '+' | '-' | '*' | '/' | '\\' | '^'
        | '~' | '=' | '<' | '>' | '!' | '?' | '@' | '#'
        | '$' | '%' | '&' | '|' | ':' | '\'' | '`'
        => true,
        _ => false
    }
}

named!(
    white <&[u8]>,
    recognize!(many0!(alt!(multispace | comment)))
    // chain!(
    //     parts: many0!(alt!(multispace | comment)),
    //     || fuse_slices(&parts)
    // )
);

named!(
    comment <&[u8]>,
    recognize!(
        alt!(
            chain!(
                _start: tag!(";;")
                ~ not_line_ending
                ~ _end: alt!(tag!("\r\n") | tag!("\n"))
                ,
                || ()
            )
            | chain!(
                _start: tag!("(;")
                ~ many0!(
                    alt!(
                        comment
                        | take!(1)
                    )
                )
                ~ _end: tag!(";)")
                ,
                || ()
            )
        )
    )
);

fn fuse_slices<'a>(slices: &[&'a [u8]]) -> &'a [u8] {
    match slices.len() {
        0 => &[],
        1 => slices[0],
        n => unsafe {
            // TODO: Do this properly.
            let beg = slices[0].as_ptr();
            let last = slices[n-1];
            let end = last.as_ptr().offset(last.len() as isize);
            let len = end as usize - beg as usize;
            ::std::slice::from_raw_parts(beg, len)
        }
    }
}
