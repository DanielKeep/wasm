#[macro_use] extern crate nom;
extern crate wasm_model as model;

mod parse;

pub use parse::module_ as module;
