extern crate clap;
extern crate conv;
extern crate wasm_hir as hir;
extern crate wasm_hir_int as hir_int;
extern crate wasm_hir_trans as hir_trans;
extern crate wast_parse;

use conv::prelude::*;

type Error = Box<std::error::Error>;

fn main() {
    use clap::{App, Arg, SubCommand};
    let args = App::new("sandbox")
        .subcommand(SubCommand::with_name("call")
            .about("Call an exported function in the module.")
            .arg(Arg::with_name("MODULE")
                .help("The module to load.")
                .required(true)
            )
            .arg(Arg::with_name("FUNC")
                .help("Exported function name.")
                .required(true)
            )
            .arg(Arg::with_name("PARAMS")
                .help("Parameters passed to the function.")
                .multiple(true)
            )
        )
        .subcommand(SubCommand::with_name("parse")
            .about("Parse a module.")
            .arg(Arg::with_name("MODULE")
                .help("The module to parse.")
                .required(true)
            )
            .arg(Arg::with_name("no_output")
                .help("Don't output the resulting AST.")
                .long("no-output")
            )
        )
        .subcommand(SubCommand::with_name("test")
            .about("Run module asserts.")
            .arg(Arg::with_name("MODULE")
                .help("The module to test.")
                .required(true)
            )
        )
        .get_matches();

    let r = if let Some(sc_args) = args.subcommand_matches("call") {
        cmd_call(&sc_args.value_of("MODULE").unwrap(),
            &sc_args.value_of("FUNC").unwrap(),
            &sc_args.values_of("PARAMS").map(|vs| vs.collect::<Vec<_>>())
                .unwrap_or(vec![]))
    } else if let Some(sc_args) = args.subcommand_matches("parse") {
        cmd_parse(&sc_args.value_of("MODULE").unwrap(),
            !sc_args.is_present("no_output"))
    } else if let Some(sc_args) = args.subcommand_matches("test") {
        cmd_test(&sc_args.value_of("MODULE").unwrap())
    } else {
        println!("error: no command specified.");
        std::process::exit(1);
    };

    match r {
        Ok(()) => (),
        Err(err) => {
            println!("{}", err);
            std::process::exit(1);
        }
    }
}

fn cmd_call(module_path: &str, fn_name: &str, params: &[&str]) -> Result<(), Error> {
    let mut params: Vec<_> = try!(params.iter()
        .map(|param| {
            if let Ok(v) = param.parse() {
                Ok(hir::Value::I64(v))
            } else if let Ok(v) = param.parse() {
                Ok(hir::Value::F64(v))
            } else {
                Err(Error::from(format!("could not parse parameter as \
                    integer or float: {:?}", param)))
            }
        })
        .collect());

    println!("Reading...");
    let src = try!(read_file(module_path));

    println!("Parsing...");
    let wast = match wast_parse::script(&src) {
        Ok(v) => v,
        Err(err) => {
            return Err(From::from(format!("parse error: {:?}", err)));
        },
    };

    println!("Translating to HIR...");
    let hir = match hir_trans::lower_script(wast) {
        Ok(v) => v,
        Err(err) => {
            return Err(From::from(format!("hir trans error: {:?}", err)));
        },
    };

    println!("Calling {}...", fn_name);
    let spectest = SpecTestLib;
    let mut state = hir_int::State::new(&hir.modules[hir.export_module]);
    try!(state.add_import("spectest".into(), &spectest));

    let result = if let Some(func_id) = state.lookup_export(fn_name) {
        try!(state.coerce_params(func_id, &mut params));
        match state.direct_invoke_func(func_id, &params) {
            Ok(v) => v,
            Err(err) => return Err(From::from(format!(
                "hir int error: {:?}", err))),
        }
    } else {
        return Err(From::from(format!(
            "hit int error: could not find export {:?}", fn_name)));
    };

    match result {
        Some(v) => println!("Result: {:?}", v),
        None => println!("Done."),
    }
    Ok(())
}

fn cmd_parse(module_path: &str, print_ast: bool) -> Result<(), Error> {
    println!("Reading...");
    let src = try!(read_file(module_path));

    println!("Parsing...");
    let wast = match wast_parse::script(&src) {
        Ok(v) => v,
        Err(err) => {
            return Err(From::from(format!("parse error: {:?}", err)));
        },
    };

    if print_ast {
        println!("Ast: {:#?}", wast);
    }
    Ok(())
}

fn cmd_test(module_path: &str) -> Result<(), Error> {
    println!("Reading...");
    let src = try!(read_file(module_path));

    println!("Parsing...");
    let wast = match wast_parse::script(&src) {
        Ok(v) => v,
        Err(err) => {
            return Err(From::from(format!("parse error: {:?}", err)));
        },
    };

    println!("Translating to HIR...");
    let hir = match hir_trans::lower_script(wast) {
        Ok(v) => v,
        Err(err) => {
            return Err(From::from(format!("hir trans error: {:?}", err)));
        },
    };

    println!("Running asserts...");
    let spectest = SpecTestLib;
    let mut state = hir_int::State::new(&hir.modules[hir.export_module]);
    try!(state.add_import("spectest".into(), &spectest));

    match state.run_asserts(&hir.asserts) {
        Ok(()) => (),
        Err(errs) => {
            use std::io::Write;
            let mut msg = vec![];
            try!(writeln!(msg, "script assertion failures:"));
            for err in errs {
                try!(writeln!(msg, " - {}", err));
            }
            return Err(From::from(String::from_utf8(msg).unwrap()));
        }
    }

    println!("All OK.");
    Ok(())
}

fn read_file(path: &str) -> Result<Vec<u8>, Error> {
    use std::fs;
    use std::io::prelude::*;

    let mut f = try!(fs::File::open(path));
    let mut bs = vec![];
    try!(f.read_to_end(&mut bs));
    Ok(bs)
}

struct SpecTestLib;

macro_rules! sig_match {
    ($sig:expr, $params:expr;
        $name:ident($($param_names:ident: $param_tys:ty),* $(,)*)
        => $body:expr) => {
        if $sig.name == stringify!($name)
            && $sig.result == hir::Type::Void
            && $sig.params == &[
                $(<$param_tys as hir::ValueTypeEquiv>::value_type()),*
            ]
        {
            let mut params = &$params[..];
            $(
                let $param_names: $param_tys = params[0].value_into().unwrap();
                params = &params[1..];
            )*
            assert_eq!(params.len(), 0);

            $body
        }
    };
}

impl hir_int::ImportModule for SpecTestLib {
    fn call(&self, sig: &hir::ImportSig, params: &[hir::Value]) -> Result<Option<hir::Value>, hir_int::Error> {
        sig_match! { sig, params; print(a: i32) => {
            println!("{} : i32", a);
            return Ok(None);
        }}

        sig_match! { sig, params; print(a: i64) => {
            println!("{} : i64", a);
            return Ok(None);
        }}

        sig_match! { sig, params; print(a: i32, b: f32) => {
            println!("{} : i32", a);
            println!("{} : f32", b);
            return Ok(None);
        }}

        sig_match! { sig, params; print(a: i64, b: f64) => {
            println!("{} : i64", a);
            println!("{} : f64", b);
            return Ok(None);
        }}

        Err(From::from(format!("unknown signature {:?}", sig)))
    }
}
