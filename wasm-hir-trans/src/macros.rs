macro_rules! err_from {
    ($e:expr) => {
        ::std::convert::Into::into($e)
    };
}

macro_rules! throw {
    ($e:expr) => {
        return Err(err_from!($e))
    };

    ($fmt:expr, $($args:tt)*) => {
        return Err(err_from!(format!($fmt, $($args)*)))
    };
}

macro_rules! try_assert {
    ($cond:expr, $($msg:tt)*) => {
        if !$cond { throw!($($msg)*) }
    };
}

macro_rules! try_ty_eq {
    ($exp:expr, $got:expr) => {
        match ($exp, $got) {
            (exp, got) => try_assert!(exp.unifies_with(got), {
                ::ty_check_fail();
                format!("type mismatch (expected {:?}, found {:?})", exp, got)
            })
        }
    };

    ($exp:expr, $got:expr, $($msg:tt)*) => {
        match ($exp, $got) {
            // FIXME: This is horrible.
            (exp, got) => try_assert!(exp.unifies_with(got), {
                ::ty_check_fail();
                format!("type mismatch: {} (expected {:?}, found {:?})",
                    format!($($msg)*), exp, got)
            })
        }
    };
}

macro_rules! try_ty_unify {
    ($exp:expr, $got:expr, $($msg:tt)*) => {
        match ($exp, $got) {
            (exp, got) => match exp.unify_with(got) {
                Some(ty) => ty,
                None => {
                    ::ty_check_fail();
                    throw!("type mismatch: {} (expected {:?}, found {:?})",
                        format!($($msg)*), exp, got)
                },
            }
        }
    };
}
