extern crate conv;
extern crate wasm_hir as hir;
extern crate wast_parse as wast;

use std::borrow::Cow;
use std::cell::Cell;
use std::collections::HashMap;
use conv::prelude::*;
use hir::TableId;
use wast::types as tty;

#[macro_use] mod macros;

pub type Error = Box<std::error::Error>;

pub const WASM_PAGE_SIZE: u64 = 64 * 1024;

pub fn lower_script(script: tty::Script) -> Result<hir::Script, Error> {
    script.into_hir()
}

#[derive(Debug)]
struct BlockFrame<'i: 'a, 'a> {
    note: Option<Cow<'i, str>>,
    label: Option<hir::Name<'i>>,
    parent: Option<&'a BlockFrame<'i, 'a>>,
    type_: Cell<Option<hir::Type>>,
}

impl<'i: 'a, 'a> BlockFrame<'i, 'a> {
    fn merge_type(&self, type_: hir::Type) -> Result<(), Error> {
        match self.type_.get() {
            None => self.type_.set(Some(type_)),
            Some(cur_type) => self.type_.set(
                Some(try_ty_unify!(cur_type, type_,
                    "branches targeting label imply different types"))),
        }
        Ok(())
    }
}

#[derive(Copy, Clone)]
struct ExprTables<'i: 'a, 'a> {
    func_sigs: &'a FuncSigTable<'i, 'a>,
    import_sigs: &'a ImportSigTable<'i, 'a>,
    locals: &'a LocalTable<'i, 'a>,
    block_chain: Option<&'a BlockFrame<'i, 'a>>,
    return_ty: &'a hir::Type,
}

impl<'i: 'a, 'a> ExprTables<'i, 'a> {
    fn resolve_branch_target(&self, target: &tty::Var<'i>)
    -> Result<(hir::BranchDepth, &'a BlockFrame<'i, 'a>), Error> {
        let mut depth = 0;
        let mut next = self.block_chain;
        while let Some(cur) = next {
            match *target {
                tty::Var::Id(id) => {
                    if id == depth.value_as::<tty::Id>().unwrap_ok() {
                        return Ok((depth, cur));
                    }
                },
                tty::Var::Name(ref name) => {
                    if let Some(ref lbl) = cur.label {
                        if &lbl[..] == &name[..] {
                            return Ok((depth, cur));
                        }
                    }
                }
            }

            depth = try!(depth.checked_add(1)
                .ok_or_else(|| format!(
                    "branch depth limit exceeded while searching for \
                        target `{:?}`",
                    target)));
            next = cur.parent;
        }
        throw!("could not locate branch target `{:?}`", target);
    }

    fn with_block_frame<'b>(&'b self, block_frame: &'b BlockFrame<'i, 'b>) -> ExprTables<'i, 'b> {
        ExprTables {
            func_sigs: self.func_sigs,
            import_sigs: self.import_sigs,
            locals: self.locals,
            block_chain: Some(block_frame),
            return_ty: self.return_ty,
        }
    }
}

#[derive(Copy, Clone)]
struct FuncTables<'i: 'a, 'a> {
    func_sigs: &'a FuncSigTable<'i, 'a>,
    import_sigs: &'a ImportSigTable<'i, 'a>,
}

type ExportTable<'i, 'a> = NamedTable<'i, 'a, hir::Func<'i>>;
type FuncSigTable<'i, 'a> = NamedTable<'i, 'a, HirFuncSig<'i>>;
type ImportSigTable<'i, 'a> = NamedTable<'i, 'a, HirImportSig>;
type LocalTable<'i, 'a> = NamedTable<'i, 'a, HirLocal>;

struct HirFuncSig<'i> {
    result: hir::Type,
    params: Vec<hir::Binding<'i>>,
}

impl<'i> hir::TableEntry for HirFuncSig<'i> {
    type Id = hir::FuncId;
}

struct HirImportSig {
    result: hir::Type,
    params: Vec<hir::ValueType>,
}

impl<'i> hir::TableEntry for HirImportSig {
    type Id = hir::ImportId;
}

struct HirLocal {
    type_: hir::ValueType,
}

impl hir::TableEntry for HirLocal {
    type Id = hir::LocalId;
}

struct WastFuncSig<'i>(Option<tty::ValueType>, Vec<tty::Binding<'i>>);

struct WastImportSig(Option<tty::ValueType>, Vec<tty::ValueType>);

trait IntoHir<'i, Ctx>: Sized {
    type Hir: 'i;

    fn into_hir(self) -> Result<Self::Hir, Error> where Ctx: Default {
        self.into_hir_with(Ctx::default())
    }

    fn into_hir_with(self, Ctx) -> Result<Self::Hir, Error>;
}

impl<'i, 'a> IntoHir<'i, &'a ExportTable<'i, 'a>> for tty::AssertReturn<'i> {
    type Hir = hir::Assert<'i>;

    fn into_hir_with(self, exports: &'a ExportTable<'i, 'a>) -> Result<Self::Hir, Error> {
        let func_id = try!(exports.resolve_name(&self.fn_name));
        let func = try!(exports.lookup(func_id));
        let params = try!(self.params.into_hir());
        let cmp_eq = try!(self.cmp_eq.map(IntoHir::into_hir).swap());

        try_assert!(func.params.len() == params.len(),
            format!("parameter mismatch (got `{}` params, expected `{}`)",
                func.params.len(), params.len()));

        for (i, (fn_p, call_p)) in func.params.iter().zip(params.iter()).enumerate() {
            try_ty_eq!(hir::Type::from(fn_p.type_), call_p.type_(),
                "parameter {}", i);
        }

        let cmp_ty = cmp_eq.map(|v| v.type_()).unwrap_or(hir::Type::Void);
        try_ty_eq!(func.result, cmp_ty, "return type");

        Ok(hir::Assert::Return(self.fn_name, params, cmp_eq))
    }
}

impl<'i, 'a> IntoHir<'i, &'a ExportTable<'i, 'a>> for tty::AssertTrap<'i> {
    type Hir = hir::Assert<'i>;

    fn into_hir_with(self, exports: &'a ExportTable<'i, 'a>) -> Result<Self::Hir, Error> {
        let func_id = try!(exports.resolve_name(&self.fn_name));
        let func = try!(exports.lookup(func_id));
        let params = try!(self.params.into_hir());

        try_assert!(func.params.len() == params.len(),
            format!("parameter mismatch (got `{}` params, expected `{}`)",
                func.params.len(), params.len()));

        for (i, (fn_p, call_p)) in func.params.iter().zip(params.iter()).enumerate() {
            try_ty_eq!(hir::Type::from(fn_p.type_), call_p.type_(),
                "parameter {}", i);
        }

        Ok(hir::Assert::Trap(self.fn_name, params, self.trap_msg))
    }
}

impl<'i> IntoHir<'i, ()> for tty::Binary {
    type Hir = hir::Binary;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        use wast::types::Binary as TB;
        use hir::Binary as HB;
        Ok(match self {
            TB::I32Add => HB::I32Add,
            TB::I32And => HB::I32And,
            TB::I32Mul => HB::I32Mul,
            TB::I32Sub => HB::I32Sub,

            TB::I64Add => HB::I64Add,
            TB::I64And => HB::I64And,
            TB::I64Mul => HB::I64Mul,
            TB::I64Sub => HB::I64Sub,
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Binding<'i> {
    type Hir = hir::Binding<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let name = try!(self.name.into_hir());
        let type_ = try!(self.type_.into_hir());
        Ok(hir::Binding {
            name: name,
            type_: type_,
        })
    }
}

impl<'i, 'a> IntoHir<'i, &'a ExprTables<'i, 'a>> for tty::Branch<'i> {
    type Hir = hir::Expr;

    fn into_hir_with(self, tables: &'a ExprTables<'i, 'a>) -> Result<Self::Hir, Error> {
        let tty::Branch { label: m_label, body: exprs } = self;
        let (exprs, block_ty) = {
            let m_label = try!(m_label.map(IntoHir::into_hir).swap());
            let block_frame = BlockFrame {
                note: Some("branch block".into()),
                label: m_label,
                parent: tables.block_chain,
                type_: Cell::new(None),
            };

            let expr_tables = &tables.with_block_frame(&block_frame);
            let exprs = try!(exprs.into_hir_with(expr_tables));

            (exprs, block_frame.type_.get())
        };

        let ty = if let Some(last_expr) = exprs.last() {
            let ty = last_expr.type_;
            if let Some(block_ty) = block_ty {
                try_ty_eq!(ty, block_ty,
                    "result of branch inconsistent with jumps");
            }
            ty
        } else {
            assert!(block_ty.is_none(), "branch has no expressions, but *does* have a block_ty; wat :|");
            hir::Type::Void
        };

        Ok(hir::Expr {
            type_: ty,
            node: hir::ExprNode::Block(exprs),
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Compare {
    type Hir = hir::Compare;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        use wast::types::Compare as TC;
        use hir::Compare as HC;
        Ok(match self {
            TC::F32Eq => HC::F32Eq,
            TC::F32Ne => HC::F32Ne,
            TC::F64Eq => HC::F64Eq,
            TC::F64Ne => HC::F64Ne,
            TC::I32Eq => HC::I32Eq,
            TC::I32Ne => HC::I32Ne,
            TC::I64Eq => HC::I64Eq,
            TC::I64Ne => HC::I64Ne,
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Const {
    type Hir = hir::Type;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        use wast::types::Const as TC;
        use hir::Type as HT;
        use hir::ValueType as HV;
        Ok(match self {
            TC::I32 => HT::Value(HV::I32),
            TC::I64 => HT::Value(HV::I64),
            TC::F32 => HT::Value(HV::F32),
            TC::F64 => HT::Value(HV::F64),
        })
    }
}

impl<'i> IntoHir<'i, ()> for (tty::Const, tty::Value) {
    type Hir = hir::Value;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let (kind, val) = self;
        let ty = try!(kind.into_hir());
        let val = try!(val.into_hir());
        try_ty_eq!(ty, val.type_());
        Ok(val)
    }
}

impl<'i, 'a> IntoHir<'i, &'a FuncSigTable<'i, 'a>> for tty::Export<'i> {
    type Hir = hir::Export<'i>;

    fn into_hir_with(self, func_sigs: &'a FuncSigTable<'i, 'a>) -> Result<Self::Hir, Error> {
        match self {
            tty::Export::Memory(..) => panic!("nyi"),
            tty::Export::Var(name, var) => {
                Ok(hir::Export {
                    name: name,
                    func: try!(func_sigs.resolve(&var)),
                })
            },
        }
    }
}

impl<'i, 'a> IntoHir<'i, &'a ExprTables<'i, 'a>> for tty::Expr<'i> {
    type Hir = hir::Expr;

    fn into_hir_with(self, tables: &'a ExprTables<'i, 'a>) -> Result<Self::Hir, Error> {
        use wast::types::Expr as E;
        match self {
            E::Binary(op, lhs, rhs) => {
                let op = try!(op.into_hir());
                let (oty, lty, rty) = op.types();
                let lhs = try!(lhs.into_hir_with(tables));
                let rhs = try!(rhs.into_hir_with(tables));
                try_ty_eq!(lty, lhs.type_);
                try_ty_eq!(rty, rhs.type_);
                Ok(hir::Expr {
                    type_: oty,
                    node: hir::ExprNode::Binary(op, Box::new(lhs), Box::new(rhs)),
                })
            },

            E::Block(tty::Branch { label: m_label, body: exprs })
            | E::Label(tty::Branch { label: m_label, body: exprs })
            => {
                let (exprs, block_ty) = {
                    let m_label = try!(m_label.map(IntoHir::into_hir).swap());
                    let block_frame = BlockFrame {
                        note: Some("block/label".into()),
                        label: m_label,
                        parent: tables.block_chain,
                        type_: Cell::new(None),
                    };

                    let expr_tables = &tables.with_block_frame(&block_frame);
                    let exprs = try!(exprs.into_hir_with(expr_tables));

                    (exprs, block_frame.type_.get())
                };

                let ty = if let Some(last_expr) = exprs.last() {
                    let ty = last_expr.type_;
                    if let Some(block_ty) = block_ty {
                        try_ty_eq!(ty, block_ty,
                            "result of block inconsistent with branches");
                    }
                    ty
                } else {
                    assert!(block_ty.is_none(), "block has no expressions, but *does* have a block_ty; wat :|");
                    hir::Type::Void
                };

                Ok(hir::Expr {
                    type_: ty,
                    node: hir::ExprNode::Block(exprs),
                })
            },

            E::Br(target, result) => {
                let (branch, frame) = try!(tables.resolve_branch_target(&target));
                let result = try!(result.map(|e| e.into_hir_with(tables)).swap());

                let result_ty = result.as_ref()
                    .map(|e| e.type_)
                    .unwrap_or(hir::Type::Void);

                try!(frame.merge_type(result_ty));
                
                Ok(hir::Expr {
                    type_: hir::Type::Divergent,
                    node: hir::ExprNode::Br(branch, result.map(Box::new)),
                })
            },

            E::Call(var, params) => {
                let func_id = try!(tables.func_sigs.resolve(&var));
                let func_sig = try!(tables.func_sigs.lookup(func_id));
                let params = try!(params.into_hir_with(tables));

                try_assert!(func_sig.params.len() == params.len(),
                    format!("parameter mismatch (got `{}` params, expected `{}`)",
                        func_sig.params.len(), params.len()));

                for (i, (fn_p, call_p)) in func_sig.params.iter().zip(params.iter()).enumerate() {
                    try_ty_eq!(hir::Type::from(fn_p.type_), call_p.type_,
                        "parameter {}", i);
                }

                Ok(hir::Expr {
                    type_: func_sig.result,
                    node: hir::ExprNode::Call(func_id, params),
                })
            },

            E::CallImport(var, params) => {
                let import_id = try!(tables.import_sigs.resolve(&var));
                let import_sig = try!(tables.import_sigs.lookup(import_id));
                let params = try!(params.into_hir_with(tables));

                try_assert!(import_sig.params.len() == params.len(),
                    format!("parameter mismatch (got `{}` params, expected `{}`)",
                        import_sig.params.len(), params.len()));

                for (i, (&im_p, call_p)) in import_sig.params.iter().zip(params.iter()).enumerate() {
                    try_ty_eq!(hir::Type::from(im_p), call_p.type_,
                        "parameter {}", i);
                }

                Ok(hir::Expr {
                    type_: import_sig.result,
                    node: hir::ExprNode::CallImport(import_id, params),
                })
            },

            E::Compare(op, lhs, rhs) => {
                let op = try!(op.into_hir());
                let (lty, rty) = op.types();
                let lhs = try!(lhs.into_hir_with(tables));
                let rhs = try!(rhs.into_hir_with(tables));
                try_ty_eq!(lty, lhs.type_);
                try_ty_eq!(rty, rhs.type_);
                Ok(hir::Expr {
                    type_: hir::Type::Value(hir::ValueType::I32),
                    node: hir::ExprNode::Compare(op, Box::new(lhs), Box::new(rhs)),
                })
            },

            E::Const(kind, val) => {
                let val = try!((kind, val).into_hir());
                Ok(hir::Expr {
                    type_: val.type_(),
                    node: hir::ExprNode::Const(val),
                })
            },

            E::If(cond, then, m_else) => {
                let cond = try!(cond.into_hir_with(tables));
                try_ty_eq!(cond.type_, hir::Type::Value(hir::ValueType::I32),
                    "if condition");

                let then = try!(then.into_hir_with(tables));
                let m_else = try!(m_else.map(|else_| else_.into_hir_with(tables)).swap());

                let ty = if let Some(ref else_) = m_else {
                    try_ty_unify!(then.type_, else_.type_, "if branches")
                } else {
                    // The type of `then` doesn't matter; because we have no else branch, we're just going to throw the result away.
                    hir::Type::Void
                };

                Ok(hir::Expr {
                    type_: ty,
                    node: hir::ExprNode::If(
                        Box::new(cond),
                        Box::new(then),
                        m_else.map(Box::new),
                    ),
                })
            },

            E::IfElseNoBlock(cond, then, else_) => {
                let cond = try!(cond.into_hir_with(tables));
                try_ty_eq!(cond.type_, hir::Type::Value(hir::ValueType::I32),
                    "if condition");

                let then = try!(then.into_hir_with(tables));
                let else_ = try!(else_.into_hir_with(tables));

                let ty = try_ty_unify!(then.type_, else_.type_, "if branches");

                Ok(hir::Expr {
                    type_: ty,
                    node: hir::ExprNode::If(
                        Box::new(cond),
                        Box::new(then),
                        Some(Box::new(else_)),
                    ),
                })
            },

            E::GetLocal(var) => {
                let local_id = try!(tables.locals.resolve(&var));
                let local = try!(tables.locals.lookup(local_id));
                Ok(hir::Expr {
                    type_: hir::Type::Value(local.type_),
                    node: hir::ExprNode::GetLocal(local_id),
                })
            },

            E::Load(kind, m_offset, m_align, addr) => {
                let (kind, align) = try!((kind, m_align).into_hir());
                let offset = m_offset.unwrap_or(0);
                let addr = try!(addr.into_hir_with(tables));

                try_ty_eq!(hir::Arch::Wasm32.address_type(), addr.type_,
                    "load address");

                Ok(hir::Expr {
                    type_: kind.type_(),
                    node: hir::ExprNode::Load(
                        kind, offset, align, Box::new(addr)),
                })
            },

            E::Loop(labels, exprs) => {
                let (exprs, block_ty) = {
                    let (cont_label, exit_label) = try!(labels.into_hir());
                    let outer_frame = BlockFrame {
                        note: Some("loop exit".into()),
                        label: exit_label,
                        parent: tables.block_chain,
                        type_: Cell::new(None),
                    };
                    let inner_frame = BlockFrame {
                        note: Some("loop cont".into()),
                        label: cont_label,
                        parent: Some(&outer_frame),
                        type_: Cell::new(Some(hir::Type::Void)),
                    };

                    let expr_tables = &tables.with_block_frame(&inner_frame);
                    let exprs = try!(exprs.into_hir_with(expr_tables));

                    (exprs, outer_frame.type_.get())
                };

                let ty = exprs.last().unwrap().type_;
                if let Some(block_ty) = block_ty {
                    try_ty_eq!(ty, block_ty,
                        "result of loop inconsistent with branches");
                }

                Ok(hir::Expr {
                    type_: ty,
                    node: hir::ExprNode::Block(vec![
                        hir::Expr {
                            type_: hir::Type::Divergent,
                            node: hir::ExprNode::Loop(exprs),
                        },
                    ]),
                })
            },

            E::Return(m_expr) => {
                let m_expr = try!(m_expr
                    .map(|e| e.into_hir_with(tables))
                    .swap());

                let expr_ty = m_expr.as_ref()
                    .map(|e| e.type_)
                    .unwrap_or(hir::Type::Void);

                try_ty_eq!(tables.return_ty, expr_ty,
                    "return expression does not match return type");

                Ok(hir::Expr {
                    type_: hir::Type::Divergent,
                    node: hir::ExprNode::Return(m_expr.map(Box::new)),
                })
            },

            E::SetLocal(var, expr) => {
                let local_id = try!(tables.locals.resolve(&var));
                let local = try!(tables.locals.lookup(local_id));
                let expr = try!(expr.into_hir_with(tables));

                try_ty_eq!(hir::Type::Value(local.type_), expr.type_,
                    "local variable {:?}", var);

                Ok(hir::Expr {
                    type_: expr.type_,
                    node: hir::ExprNode::SetLocal(local_id, Box::new(expr)),
                })
            },

            E::Store(kind, m_offset, m_align, addr, value) => {
                let (kind, align) = try!((kind, m_align).into_hir());
                let offset = m_offset.unwrap_or(0);
                let addr = try!(addr.into_hir_with(tables));
                let value = try!(value.into_hir_with(tables));

                try_ty_eq!(hir::Arch::Wasm32.address_type(), addr.type_,
                    "store address");
                try_ty_eq!(kind.type_(), value.type_, "store value");

                Ok(hir::Expr {
                    type_: kind.type_(),
                    node: hir::ExprNode::Store(
                        kind, offset, align,
                        Box::new(addr),
                        Box::new(value),
                    )
                })
            },

            E::Unary(op, arg) => {
                let op = try!(op.into_hir());
                let (oty, aty) = op.types();
                let arg = try!(arg.into_hir_with(tables));
                try_ty_eq!(aty, arg.type_);
                Ok(hir::Expr {
                    type_: oty,
                    node: hir::ExprNode::Unary(op, Box::new(arg)),
                })
            },
        }
    }
}

impl<'i, 'a> IntoHir<'i, &'a FuncTables<'i, 'a>> for tty::Func<'i> {
    type Hir = hir::Func<'i>;

    fn into_hir_with(self, tables: &'a FuncTables<'i, 'a>) -> Result<Self::Hir, Error> {
        let name = try!(self.name.into_hir());
        let result = try!(self.result.into_hir());

        // Construct the locals table.
        let mut local_names: HashMap<Cow<str>, _> = HashMap::new();
        let mut local_tbl = Vec::with_capacity(self.params.len() + self.locals.len());
        for (i, param) in self.params.iter().enumerate() {
            if let Some(ref name) = param.name {
                let name = name;
                try_assert!(!local_names.contains_key(&name[..]),
                    format!("duplicate parameter name (`{}`)", name));
                local_names.insert(name.clone().into(), hir::LocalId::from_usize(i));
            }

            local_tbl.push(HirLocal {
                type_: try!(param.type_.into_hir()),
            });
        }

        let local_offset = local_tbl.len();
        for (i, local) in self.locals.iter().enumerate() {
            let i = i + local_offset;

            if let Some(ref name) = local.name {
                let name = name;
                try_assert!(!local_names.contains_key(&name[..]),
                    format!("duplicate local name (`{}`)", name));
                local_names.insert(name.clone().into(), hir::LocalId::from_usize(i));
            }

            local_tbl.push(HirLocal {
                type_: try!(local.type_.into_hir())
            });
        }

        let local_tbl = NamedTable {
            names: &local_names,
            values: &local_tbl,
        };

        let params = try!(self.params.into_hir());
        let locals = try!(self.locals.into_hir());

        let exprs = {
            let block_frame = BlockFrame {
                note: Some("func".into()),
                label: None,
                parent: None,
                type_: Cell::new(Some(result)),
            };

            let expr_tables = ExprTables {
                func_sigs: tables.func_sigs,
                import_sigs: tables.import_sigs,
                locals: &local_tbl,
                block_chain: Some(&block_frame),
                return_ty: &result,
            };

            try!(self.exprs.into_hir_with(&expr_tables))
        };

        Ok(hir::Func {
            name: name,
            result: result,
            params: params,
            locals: locals,
            body: exprs,
        })
    }
}

impl<'i> IntoHir<'i, ()> for (tty::Load, Option<tty::Align>) {
    type Hir = (hir::Load, hir::Align);

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let (kind, align) = self;
        let (kind, nat_align) = match kind {
            tty::Load::F32 => (hir::Load::F32, 4),
            tty::Load::F64 => (hir::Load::F64, 8),
            tty::Load::I32 => (hir::Load::I32, 4),
            tty::Load::I32FromI8 => (hir::Load::I32FromI8, 1),
            tty::Load::I32FromU8 => (hir::Load::I32FromU8, 1),
            tty::Load::I32FromI16 => (hir::Load::I32FromI16, 2),
            tty::Load::I32FromU16 => (hir::Load::I32FromU16, 2),
            tty::Load::I64 => (hir::Load::I64, 8),
            tty::Load::I64FromI8 => (hir::Load::I64FromI8, 1),
            tty::Load::I64FromU8 => (hir::Load::I64FromU8, 1),
            tty::Load::I64FromI16 => (hir::Load::I64FromI16, 2),
            tty::Load::I64FromU16 => (hir::Load::I64FromU16, 2),
            tty::Load::I64FromI32 => (hir::Load::I64FromI32, 4),
            tty::Load::I64FromU32 => (hir::Load::I64FromU32, 4),
        };

        let align = align.unwrap_or(nat_align);
        if align.count_ones() != 1 {
            throw!("non-power-of-two alignment (align={})", align);
        }

        Ok((kind, align))
    }
}

impl<'i> IntoHir<'i, ()> for tty::Import<'i> {
    type Hir = hir::Import<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        Ok(hir::Import {
            name: try!(self.name.map(IntoHir::into_hir).swap()),
            module: self.import_module,
            sig: hir::ImportSig {
                name: self.import_name,
                result: try!(self.result.into_hir()),
                params: try!(self.params.into_hir()),
            },
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::LoopLabels<'i> {
    type Hir = (Option<hir::Name<'i>>, Option<hir::Name<'i>>);

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        Ok(match self {
            tty::LoopLabels::None => (None, None),
            tty::LoopLabels::Cont(cont) => (
                Some(try!(cont.into_hir())),
                None,
            ),
            tty::LoopLabels::ExitCont(exit, cont) => (
                Some(try!(cont.into_hir())),
                Some(try!(exit.into_hir())),
            ),
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Memory<'i> {
    type Hir = hir::Memory<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let min = self.min;
        let max = self.max.unwrap_or(self.min);

        if min > max {
            throw!("initial memory pages must be less than or equal to the \
                maximum (min: {}, max: {})", self.min, max);
        }

        // Numbers are in multiples of the page size.
        let min = min * WASM_PAGE_SIZE;
        let max = max * WASM_PAGE_SIZE;

        let segments = try!(self.segments.into_hir_with(min));

        let mut last_end = 0;
        for (i, segment) in segments.iter().enumerate() {
            if segment.offset < last_end {
                throw!("data segment not disjoint and ordered (segment {} \
                    offset {} not after {})", i, segment.offset, last_end);
            }
            last_end = segment.offset + (segment.data.len() as u64);
        }

        Ok(hir::Memory {
            min: min,
            max: max,
            segments: segments,
        })
    }
}

impl<'i> IntoHir<'i, ()> for Option<tty::Memory<'i>> {
    type Hir = hir::Memory<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        self.map(IntoHir::into_hir)
            .unwrap_or_else(|| Ok(hir::Memory::default()))
    }
}

impl<'i> IntoHir<'i, ()> for tty::Module<'i> {
    type Hir = hir::Module<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        use wast::types::ModuleEntry as Ent;

        let mut funcs = vec![];
        let mut exports = vec![];
        let mut imports = vec![];
        let mut memory = None;

        for entry in self.entries {
            match entry {
                Ent::Export(e) => exports.push(e),
                Ent::Func(f) => funcs.push(f),
                Ent::Import(i) => imports.push(i),
                Ent::Memory(m) => {
                    try_assert!(memory.is_none(),
                        "module must have at most one memory entry");
                    memory = Some(m);
                },
            }
        }

        let memory = try!(memory.into_hir());

        // Translate imports so they're available later.
        let mut import_names: HashMap<Cow<str>, _> = HashMap::new();
        let mut import_sigs = Vec::with_capacity(imports.len());
        for (i, import) in imports.iter().enumerate() {
            if let Some(ref name) = import.name {
                let name = name;
                try_assert!(!import_names.contains_key(&name[..]),
                    format!("duplicate import name: `{}`", name));
                import_names.insert(name.clone().into(), hir::ImportId::from_usize(i));
            }

            let sig = WastImportSig(import.result, import.params.clone());
            import_sigs.push(try!(sig.into_hir()));
        }

        let import_sigs = NamedTable {
            names: &import_names,
            values: &import_sigs,
        };

        // Translate the signatures of the functions to build a lookup table we can use to type-check `call`s.
        let mut func_names: HashMap<Cow<str>, _> = HashMap::new();
        let mut func_sigs = Vec::with_capacity(funcs.len());
        for (i, func) in funcs.iter().enumerate() {
            if let Some(ref name) = func.name {
                let name = name;
                try_assert!(!func_names.contains_key(&name[..]),
                    format!("duplicate function name: `{}`", name));
                func_names.insert(name.clone().into(), hir::FuncId::from_usize(i));
            }

            let sig = WastFuncSig(func.result.clone(), func.params.clone());
            func_sigs.push(try!(sig.into_hir()));
        }

        let func_sigs = NamedTable {
            names: &func_names,
            values: &func_sigs,
        };

        let func_tables = FuncTables {
            func_sigs: &func_sigs,
            import_sigs: &import_sigs,
        };

        // Now translate the actual functions and exports.
        let exports = try!(exports.into_hir_with(&func_sigs));
        let funcs = try!(funcs.into_hir_with(&func_tables));
        let imports = try!(imports.into_hir());

        Ok(hir::Module {
            exports: exports,
            funcs: funcs,
            imports: imports,
            memory: memory,
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Name<'i> {
    type Hir = hir::Name<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        Ok((Into::<::std::borrow::Cow<'i, str>>::into(self)).into())
    }
}

impl<'i> IntoHir<'i, ()> for Option<tty::Name<'i>> {
    type Hir = Option<hir::Name<'i>>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        self.map(IntoHir::into_hir).swap()
    }
}

impl<'i> IntoHir<'i, ()> for tty::Script<'i> {
    type Hir = hir::Script<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let mut modules = vec![];
        let mut assert_returns = vec![];
        let mut assert_traps = vec![];

        for entry in self.entries {
            match entry {
                tty::ScriptEntry::Module(m) => modules.push(try!(m.into_hir())),
                tty::ScriptEntry::AssertReturn(ar) => assert_returns.push(ar),
                tty::ScriptEntry::AssertTrap(ar) => assert_traps.push(ar),

                tty::ScriptEntry::AssertInvalid(ai) => {
                    match ai.module.into_hir() {
                        Ok(_) => throw!("invalid module wasn't invalid"),
                        Err(err) => {
                            let err_msg = format!("{}", err);
                            if !err_msg.contains(&ai.trap_msg[..]) {
                                throw!("invalid module error invalid \
                                    (expected {:?}, got {:?})",
                                    ai.trap_msg, err_msg);
                            }
                        }
                    }
                },
            }
        }

        if modules.len() == 0 {
            throw!("script must contain a module");
        }

        let (asserts, export_module) = {
            // Construct the export table for translating assertions.
            let mut export_names = HashMap::new();
            let mut funcs = &[][..];
            let mut found_exports = false;
            let mut export_module = !0;

            for (i, module) in modules.iter().enumerate() {
                if module.exports.len() > 0 {
                    if found_exports {
                        throw!("cannot have more than one module in a script with exports");
                    }
                    found_exports = true;
                    export_module = i;
                    export_names.extend(module.exports.iter()
                        .map(|e| (e.name.clone(), e.func)));
                    funcs = &module.funcs[..];
                }
            }

            let exports = &NamedTable {
                names: &export_names,
                values: funcs,
            };

            // Translate assertions.
            let mut asserts = vec![];
            for ar in assert_returns {
                asserts.push(try!(ar.into_hir_with(exports)));
            }
            for at in assert_traps {
                asserts.push(try!(at.into_hir_with(exports)));
            }
            (asserts, export_module)
        };

        Ok(hir::Script {
            modules: modules,
            asserts: asserts,
            export_module: export_module,
        })
    }
}

impl<'i> IntoHir<'i, u64> for tty::Segment<'i> {
    type Hir = hir::Segment<'i>;

    fn into_hir_with(self, min: u64) -> Result<Self::Hir, Error> {
        let end = self.offset + (self.data.len() as u64);
        if end > min {
            throw!(
                "data segment does not fit memory (memory size: {:?}, \
                    segment offset: {:?}, segment length: {:?})",
                min, self.offset, self.data/*.len()*/
            );
        }

        Ok(hir::Segment {
            offset: self.offset,
            data: self.data,
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::Unary {
    type Hir = hir::Unary;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        use wast::types::Unary as TU;
        use hir::Unary as HU;
        Ok(match self {
            TU::F64FromI32 => HU::F64FromI32,
            TU::F64FromI64Bits => HU::F64FromI64Bits,
        })
    }
}

impl<'i> IntoHir<'i, ()> for (tty::Store, Option<tty::Align>) {
    type Hir = (hir::Store, hir::Align);

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let (kind, align) = self;
        let (kind, nat_align) = match kind {
            tty::Store::F32 => (hir::Store::F32, 4),
            tty::Store::F64 => (hir::Store::F64, 8),
            tty::Store::I32 => (hir::Store::I32, 4),
            tty::Store::I32AsB8 => (hir::Store::I32AsB8, 1),
            tty::Store::I32AsB16 => (hir::Store::I32AsB16, 2),
            tty::Store::I64 => (hir::Store::I64, 8),
            tty::Store::I64AsB8 => (hir::Store::I64AsB8, 1),
            tty::Store::I64AsB16 => (hir::Store::I64AsB16, 2),
            tty::Store::I64AsB32 => (hir::Store::I64AsB32, 4),
        };

        let align = align.unwrap_or(nat_align);
        if align.count_ones() != 1 {
            throw!("non-power-of-two alignment (align={})", align);
        }

        Ok((kind, align))
    }
}

impl<'i> IntoHir<'i, ()> for tty::Value {
    type Hir = hir::Value;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        Ok(match self {
            tty::Value::I32(v) => hir::Value::I32(v),
            tty::Value::I64(v) => hir::Value::I64(v),
            tty::Value::F32(v) => hir::Value::F32(v),
            tty::Value::F64(v) => hir::Value::F64(v),
        })
    }
}

impl<'i> IntoHir<'i, ()> for tty::ValueType {
    type Hir = hir::ValueType;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        Ok(match self {
            tty::ValueType::I32 => hir::ValueType::I32,
            tty::ValueType::I64 => hir::ValueType::I64,
            tty::ValueType::F32 => hir::ValueType::F32,
            tty::ValueType::F64 => hir::ValueType::F64,
        })
    }
}

impl<'i> IntoHir<'i, ()> for Option<tty::ValueType> {
    type Hir = hir::Type;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        match self {
            Some(vt) => vt.into_hir().map(hir::Type::Value),
            None => Ok(hir::Type::Void),
        }
    }
}

impl<'i> IntoHir<'i, ()> for WastFuncSig<'i> {
    type Hir = HirFuncSig<'i>;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let WastFuncSig(result, params) = self;
        Ok(HirFuncSig {
            result: try!(result.into_hir()),
            params: try!(params.into_hir()),
        })
    }
}

impl<'i> IntoHir<'i, ()> for WastImportSig {
    type Hir = HirImportSig;

    fn into_hir_with(self, _: ()) -> Result<Self::Hir, Error> {
        let WastImportSig(result, params) = self;
        Ok(HirImportSig {
            result: try!(result.into_hir()),
            params: try!(params.into_hir()),
        })
    }
}

impl<'i, Ctx, T> IntoHir<'i, Ctx> for Vec<T>
where
    T: IntoHir<'i, Ctx>,
    Ctx: Copy,
{
    type Hir = Vec<T::Hir>;

    fn into_hir_with(self, ctx: Ctx) -> Result<Self::Hir, Error> {
        self.into_iter()
            .map(|e| e.into_hir_with(ctx))
            .collect()
    }
}

struct NamedTable<'i: 'a, 'a, T>
where T: 'a + hir::TableEntry {
    names: &'a HashMap<Cow<'i, str>, T::Id>,
    values: &'a [T],
}

impl<'i: 'a, 'a, T> NamedTable<'i, 'a, T>
where T: hir::TableEntry {
    pub fn lookup(&self, id: T::Id) -> Result<&'a T, Error> {
        let idx = id.into_usize();
        self.values.get(idx)
            .ok_or_else(|| err_from!(format!("table id out of bounds (got `{}`, max is `{}`)", idx, self.values.len())))
    }

    pub fn resolve(&self, var: &tty::Var<'i>) -> Result<T::Id, Error> {
        match *var {
            tty::Var::Id(i) => {
                let i = i.value_as::<usize>().unwrap();
                if i < self.values.len() {
                    Ok(T::Id::from_usize(i))
                } else {
                    throw!(format!("var id out of bounds (got `{}`, max is `{}`)", i, self.values.len()));
                }
            },
            tty::Var::Name(ref s) => self.resolve_name(&s[..]),
        }
    }

    pub fn resolve_name(&self, name: &str) -> Result<T::Id, Error> {
        if let Some(&i) = self.names.get(name) {
            Ok(i)
        } else {
            throw!(format!("name not defined (`{}`)", name));
        }
    }
}

trait OptionResultExt: Sized {
    type T;
    type E;

    fn swap(self) -> Result<Option<Self::T>, Self::E>;
}

impl<T, E> OptionResultExt for Option<Result<T, E>> {
    type T = T;
    type E = E;

    fn swap(self) -> Result<Option<T>, E> {
        match self {
            Some(Ok(v)) => Ok(Some(v)),
            Some(Err(e)) => Err(e),
            None => Ok(None),
        }
    }
}

#[doc(hidden)]
#[inline(never)]
#[no_mangle]
pub extern "C" fn ty_check_fail() {}
