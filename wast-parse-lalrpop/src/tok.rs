use std::error::Error;
use std::str::FromStr;
use regex::Regex;

macro_rules! lexer {
    (
        $(#[$($attrs:tt)*])*
        pub enum $name:ident<$lt:tt>: $($tail:tt)*
    ) => {
        lexer! {
            @parse
            {
                name: $name,
                lt: $lt,
                attrs: {$(#[$($attrs)*])*},
            },
            {
                ignore: {(s) },
                vars: {},
                lex: {(s) },
                is_ident_cont: {is_ident_cont (_c) false},
            },
            .. $($tail)*
        }
    };

    { @as_items $($i:item)* } => { $($i)* };

    {
        @emit
        {
            name: $name:ident,
            lt: $lt:tt,
            attrs: {$($attrs:tt)*},
        },
        {
            ignore: {($ignore_arg:ident) $($ignore:tt)*},
            vars: {$($vars:tt)*},
            lex: {($lex_arg:ident) $($lex:tt)*},
            is_ident_cont: {$iic:ident ($iic_arg:ident) $iic_body:expr},
        }
    } => {
        lexer! {
            @as_items
            $($attrs)*
            pub enum $name<$lt> {
                $($vars)*
            }

            impl<$lt> $name<$lt> {
                pub fn lex_one_from(s: &$lt str) -> Result<Option<(usize, $name<$lt>, usize)>, Box<Error>> {
                    {
                        // let limit = ::std::cmp::min(16, s.len());
                        // println!(".. lex_one_from({:?}...)", &s[..limit]);
                    }

                    fn $iic($iic_arg: char) -> bool {
                        $iic_body
                    }

                    fn ignore($ignore_arg: &str) -> Result<usize, Box<Error>> {
                        // println!("   .. ignore(_)");
                        { $($ignore)* }
                        // println!("      .. fallback: 0");
                        Ok(0)
                    }

                    fn lex($lex_arg: &str) -> Result<Option<($name, usize)>, Box<Error>> {
                        {
                            use self::$name::*;
                            { $($lex)* }
                        }
                        Err(From::from(format!("unexpected character: {:?}", $lex_arg.chars().next().unwrap())))
                    }

                    let mut start = 0;
                    loop {
                        match try!(ignore(&s[start..])) {
                            0 => break,
                            n => start += n
                        }
                    }
                    // println!("   .. ignored {} bytes", start);
                    let s = &s[start..];

                    if s.len() == 0 {
                        Ok(None)
                    } else {
                        lex(s).map(|m_v_end| m_v_end
                            .map(|(v, end)| (start, v, end+start)))
                    }
                }
            }
        }
    };

    {
        @parse
        $prefix:tt,
        $args:tt,
        ..
    } => {
        lexer! { @emit $prefix, $args }
    };

    {
        @parse
        $prefix:tt,
        {
            ignore: $ignore:tt,
            vars: $vars:tt,
            lex: $lex:tt,
            is_ident_cont: {$iic:ident $($_iic_tail:tt)*},
            $($rest:tt)*
        },
        .. is_ident_cont = |$iic_arg:ident| $iic_body:expr, $($tail:tt)*
    } => {
        lexer! {
            @parse
            $prefix,
            {
                ignore: $ignore,
                vars: $vars,
                lex: $lex,
                is_ident_cont: {$iic ($iic_arg) $iic_body},
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        $prefix:tt,
        {
            ignore: {($i:ident) $($ignore:tt)*},
            $($rest:tt)*
        },
        .. ignore = re/$re:tt, $($tail:tt)*
    } => {
        lexer! {
            @parse
            $prefix,
            {
                ignore: { ($i) $($ignore)* {
                    // println!("      .. ignore re/r##\"{}\"##", $re);
                    lazy_static! {
                        static ref RE: Regex = Regex::new(concat!("^", $re))
                            .unwrap();
                    }
                    if let Some((_, end)) = RE.find($i) {
                        return Ok(end);
                    }
                } },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        $prefix:tt,
        {
            ignore: {($i:ident) $($ignore:tt)*},
            $($rest:tt)*
        },
        .. ignore = delimited $beg:tt ... $end:tt, $($tail:tt)*
    } => {
        lexer! {
            @parse
            $prefix,
            {
                ignore: { ($i) $($ignore)* {
                    // println!("      .. ignore delimited {:?} ... {:?}", $beg, $end);
                    if $i.starts_with($beg) {
                        let mut depth = 1;
                        // TODO: THIS IS HORRIBLE!
                        let len = $i.len();
                        let mut offset = $beg.len();
                        loop {
                            if offset + $end.len() > len {
                                return Err(Into::into(format!("unterminated delimited comment (still at depth {})", depth)));
                            }
                            let s = &$i[offset..];
                            if s.starts_with($beg) {
                                depth += 1;
                                offset += $beg.len();
                            } else if s.starts_with($end) {
                                depth -= 1;
                                offset += $end.len();
                                if depth == 0 {
                                    return Ok(offset);
                                }
                            } else {
                                offset += 1;
                            }
                        }
                    }
                } },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        {
            name: $name:ident,
            $($prefix_rest:tt)*
        },
        {
            ignore: $ignore:tt,
            vars: $vars:tt,
            lex: {($i:ident) $($lex:tt)*},
            $($rest:tt)*
        },
        .. ... = re/$re:tt &=> |$cl_arg:ident| $cl_body:expr,
            $($tail:tt)*
    } => {
        lexer! {
            @parse
            {
                name: $name,
                $($prefix_rest)*
            },
            {
                ignore: $ignore,
                vars: $vars,
                lex: {
                    ($i) $($lex)*
                    lexer!(@lex_re ($i) -> $name, $re &=> |$cl_arg| $cl_body);
                },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        $prefix:tt,
        {
            ignore: $ignore:tt,
            vars: {$($vars:tt)*},
            $($rest:tt)*
        },
        .. $var:ident($($var_tys:tt)*) = ..., $($tail:tt)*
    } => {
        lexer! {
            @parse
            $prefix,
            {
                ignore: $ignore,
                vars: {$($vars)* $var($($var_tys)*), },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        $prefix:tt,
        {
            ignore: $ignore:tt,
            vars: {$($vars:tt)*},
            lex: {($i:ident) $($lex:tt)*},
            is_ident_cont: {$iic:ident $($iic_tail:tt)*},
            $($rest:tt)*
        },
        .. $var:ident = $lit:tt, $($tail:tt)*
    } => {
        lexer! {
            @parse
            $prefix,
            {
                ignore: $ignore,
                vars: {$($vars)* $var, },
                lex: { ($i) $($lex)* {
                    if $i.starts_with($lit) && !(
                        $iic($lit.chars().next_back().unwrap())
                        && $iic($i[$lit.len()..].chars().next().unwrap())
                    ) {
                        return Ok(Some(($var, $lit.len())));
                    }
                } },
                is_ident_cont: {$iic $($iic_tail)*},
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        {
            name: $name:ident,
            $($prefix_rest:tt)*
        },
        {
            ignore: $ignore:tt,
            vars: {$($vars:tt)*},
            lex: {($i:ident) $($lex:tt)*},
            $($rest:tt)*
        },
        .. $var:ident($($var_tys:tt)*) = re/$re:tt,
            $($tail:tt)*
    } => {
        lexer! {
            @parse
            {
                name: $name,
                $($prefix_rest)*
            },
            {
                ignore: $ignore,
                vars: {$($vars)* $var($($var_tys)*), },
                lex: {
                    ($i) $($lex)*
                    lexer!(@lex_re ($i) -> $name, $re &=> |s| Ok($var(s)));
                },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @parse
        {
            name: $name:ident,
            $($prefix_rest:tt)*
        },
        {
            ignore: $ignore:tt,
            vars: {$($vars:tt)*},
            lex: {($i:ident) $($lex:tt)*},
            $($rest:tt)*
        },
        .. $var:ident($($var_tys:tt)*) = re/$re:tt &=> |$cl_arg:ident| $cl_body:expr,
            $($tail:tt)*
    } => {
        lexer! {
            @parse
            {
                name: $name,
                $($prefix_rest)*
            },
            {
                ignore: $ignore,
                vars: {$($vars)* $var($($var_tys)*), },
                lex: {
                    ($i) $($lex)*
                    lexer!(@lex_re ($i) -> $name, $re &=> |$cl_arg| $cl_body);
                },
                $($rest)*
            },
            .. $($tail)*
        }
    };

    {
        @lex_re ($i:ident) -> $name:ident, $re:tt &=> |$cl_arg:ident| $cl_body:expr
    } => {
        {
            lazy_static! {
                static ref RE: Regex = Regex::new(concat!("^", $re)).unwrap();
            }
            if let Some(cap) = RE.captures($i) {
                let s_sl = if let Some(s) = cap.at(1) {
                    s
                } else {
                    cap.at(0).unwrap()
                };

                fn fn_hint<F: FnOnce(&str) -> ::std::result::Result<$name, Box<Error>>>(f: F) -> F { f }

                let cl = fn_hint(|$cl_arg| $cl_body);

                let r = cl(s_sl).map(|tok| {
                    let end = cap.pos(0).unwrap().1;
                    Some((tok, end))
                });

                return r;
            }
        }
    };
}

lexer! { #[derive(Debug)] pub enum Tok<'i>:
    is_ident_cont = |c| {
        if c > '\x7f' { return false; }
        match c as u8 {
            b'A'...b'Z' | b'a'...b'z' | b'0'...b'9'
            | b'_' | b'.' | b'-' | b'/'
            => true,
            _ => false
        }
    },

    ignore = re/r#"\s+"#,
    ignore = re/r#";;[^\r\n]*(\n|\r\n?)"#,
    ignore = delimited "(;" ... ";)",

    LParen = "(",
    RParen = ")",

    Align(u16) = re/r#"align=([0-9]+)\b"# &=> |s| err_into!(u16::from_str(s).map(Align)),
    AssertInvalid = "assert_invalid",
    AssertReturn = "assert_return",
    AssertTrap = "assert_trap",
    Block = "block",
    Br = "br",
    BrIf = "br_if",
    BrTable = "br_table",
    Call = "call",
    CallImport = "call_import",
    CallIndirect = "call_indirect",
    Else = "else",
    Export = "export",

    F32 = "f32",
    F32Const = "f32.const",
    F32Eq = "f32.eq",
    F32Load = "f32.load",
    F32Ne = "f32.ne",
    F32Store = "f32.store",

    F64 = "f64",
    F64Const = "f64.const",
    F64ConvertSI32 = "f64.convert_s/i32",
    F64Eq = "f64.eq",
    F64Load = "f64.load",
    F64Ne = "f64.ne",
    F64ReinterpretI64 = "f64.reinterpret/i64",
    F64Store = "f64.store",

    Func = "func",
    GetLocal = "get_local",
    GrowMemory = "grow_memory",

    I32 = "i32",
    I32Add = "i32.add",
    I32And = "i32.and",
    I32Const = "i32.const",
    I32Eq = "i32.eq",
    I32Load = "i32.load",
    I32Load8S = "i32.load8_s",
    I32Load8U = "i32.load8_u",
    I32Load16S = "i32.load16_s",
    I32Load16U = "i32.load16_u",
    I32Mul = "i32.mul",
    I32Ne = "i32.ne",
    I32Store = "i32.store",
    I32Store8 = "i32.store8",
    I32Store16 = "i32.store16",
    I32Sub = "i32.sub",

    I64 = "i64",
    I64Add = "i64.add",
    I64And = "i64.and",
    I64Const = "i64.const",
    I64Eq = "i64.eq",
    I64Load = "i64.load",
    I64Load8S = "i64.load8_s",
    I64Load8U = "i64.load8_u",
    I64Load16S = "i64.load16_s",
    I64Load16U = "i64.load16_u",
    I64Load32S = "i64.load32_s",
    I64Load32U = "i64.load32_u",
    I64Mul = "i64.mul",
    I64Ne = "i64.ne",
    I64Store = "i64.store",
    I64Store8 = "i64.store8",
    I64Store16 = "i64.store16",
    I64Store32 = "i64.store32",
    I64Sub = "i64.sub",

    If = "if",
    IfElse = "if_else", // NOT_IN_SPEC
    Import = "import",
    Invoke = "invoke",
    Label = "label", // NOT_IN_SPEC
    Load = "load",
    Local = "local",
    Loop = "loop",
    Memory = "memory",
    MemorySize = "memory_size",
    Module = "module",
    Nop = "nop",
    Offset(i64) = re/r#"align=([+-]?[0-9]+)\b"# &=> |s| err_into!(i64::from_str(s).map(Offset)),
    Param = "param",
    Result = "result",
    Return = "return",
    Segment = "segment",
    Select = "select",
    SetLocal = "set_local",
    Then = "then",
    Type = "type",
    Unreachable = "unreachable",

    Name(&'i str) = re/r#"\$([0-9A-Za-z_.+*/\^~=<>!?@#$%&|:'`-]+)"#,
    Float(f64) = re/r#"(?ix)
            [+-]?[0-9]+ [.] [0-9]+
            | [+-]?[0-9]+ ([.] [0-9]*)? e [+-]?[0-9]+
            | [+-]? 0x [0-9a-f]+ ([.] [0-9a-f]*)? p [+-]? [0-9]+
            | [+-]? infinity \b
            | [+-]? nan \b
            | [+-]? nan : 0x [0-9a-f]+ \b
        "# &=> |s| err_into!(f64::from_str(s).map(Float)),
    Int(i64) = ...,
    ... = re/r#"[+-][0-9]+"# &=> |s| err_into!(i64::from_str(s).map(Int)),
    ... = re/r#"[+-]0x[0-9a-f]+\b"# &=> |s| {
        let sign: i64 = match s.as_bytes()[0] {
            b'+' => 1,
            b'-' => -1,
            _ => unreachable!(),
        };
        let v = try!(i64::from_str_radix(&s[3..], 16));
        Ok(Int(v * sign))
    },
    UInt(u64) = ...,
    ... = re/r#"[0-9]+"# &=> |s| err_into!(u64::from_str(s).map(UInt)),
    ... = re/r#"0x[0-9a-f]+\b"# &=> |s| err_into!(u64::from_str_radix(s, 16).map(UInt)),
    String(&'i str) = re/r#""(([^\\"]+|\\[nt\\'"]|\\[:xdigit:]{2})*)""#,
}

#[derive(Clone, Debug)]
pub struct Tokeniser<'i> {
    text: &'i str,
    offset: usize,
    fused: bool,
}

impl<'i> Tokeniser<'i> {
    pub fn new(text: &'i str) -> Self {
        Tokeniser {
            text: text,
            offset: 0,
            fused: false,
        }
    }
}

impl<'i> Iterator for Tokeniser<'i> {
    type Item = Result<(usize, Tok<'i>, usize), Box<Error>>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.fused {
            return None;
        }

        match Tok::lex_one_from(&self.text[self.offset..]) {
            Ok(Some((l, v, r))) => {
                let o = self.offset;
                self.offset += r;
                Some(Ok((o+l, v, o+r)))
            },
            Ok(None) => {
                self.fused = true;
                None
            },
            Err(err) => {
                self.fused = true;
                let (line, col) = offset_to_line_and_col(self.text, self.offset);
                Some(Err(From::from(format!(
                    "{} (at {}:{})", err, line, col))))
            },
        }
    }
}

fn offset_to_line_and_col(s: &str, offset: usize) -> (u32, u32) {
    let mut line = 1;
    let mut col = 1;
    let mut s = &s[..offset];
    while s.len() > 0 {
        match s.find('\n') {
            Some(i) => {
                line += 1;
                s = &s[i+1..];
            },
            None => {
                col = (s.len() as u32) + 1;
                s = "";
            }
        }
    }
    (line, col)
}
