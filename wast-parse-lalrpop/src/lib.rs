#![recursion_limit="128"]

#[macro_use] extern crate lazy_static;
extern crate conv;
extern crate lalrpop_util;
extern crate regex;

use std::borrow::Cow;

#[macro_use] mod macros;

pub mod types;

mod tok;
mod wast;

pub type ParseError<'i> = lalrpop_util::ParseError<usize, tok::Tok<'i>, Box<std::error::Error + 'static>>;

pub fn script(s: &[u8]) -> Result<types::Script, ParseError> {
    let toks = tok::Tokeniser::new(std::str::from_utf8(s).unwrap());
    wast::parse_Script(toks)
}

pub fn module(s: &[u8]) -> Result<types::Module, ParseError> {
    let toks = tok::Tokeniser::new(std::str::from_utf8(s).unwrap());
    wast::parse_Module(toks)
}

fn moove_stir_cow_into_you8s(v: Cow<str>) -> Cow<[u8]> {
    match v {
        Cow::Borrowed(v) => Cow::Borrowed(v.as_bytes()),
        Cow::Owned(v) => Cow::Owned(v.into()),
    }
}

fn unescape_segment_data(s: Cow<str>) -> Cow<[u8]> {
    if let Some(first_escape) = s.find('\\') {
        let mut result = Vec::with_capacity(s.len());
        result.extend_from_slice(s[..first_escape].as_bytes());
        let mut s = &s[first_escape..];
        loop {
            if s.len() < 3 {
                result.extend_from_slice(s.as_bytes());
                break;
            }
            match s.as_bytes()[0] {
                b'\\' => {
                    result.push(u8::from_str_radix(&s[1..3], 16)
                        .expect("invalid hex escape in segment data"));
                    s = &s[3..];
                },
                _ => {
                    if let Some(next_escape) = s.find('\\') {
                        result.extend_from_slice(s[..next_escape].as_bytes());
                        s = &s[next_escape..];
                    } else {
                        result.extend_from_slice(s.as_bytes());
                        break;
                    }
                }
            }
        }
        result.into()
    } else {
        moove_stir_cow_into_you8s(s)
    }
}
