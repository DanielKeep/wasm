use std::borrow::Cow;
use std::fmt;
use std::ops::Deref;

pub type Align = u16;
pub type Id = u64;
pub type Offset = i64;
pub type Size = u64;

#[derive(Clone, Debug)]
pub struct AssertInvalid<'i> {
    pub module: Module<'i>,
    pub trap_msg: Cow<'i, str>,
}

#[derive(Clone, Debug)]
pub struct AssertReturn<'i> {
    pub fn_name: Cow<'i, str>,
    pub params: Vec<(Const, Value)>,
    pub cmp_eq: Option<(Const, Value)>,
}

#[derive(Clone, Debug)]
pub struct AssertTrap<'i> {
    pub fn_name: Cow<'i, str>,
    pub params: Vec<(Const, Value)>,
    pub trap_msg: Cow<'i, str>,
}

#[derive(Copy, Clone, Debug)]
pub enum Binary {
    I32Add,
    I32And,
    I32Mul,
    I32Sub,
    I64Add,
    I64And,
    I64Mul,
    I64Sub,
    // <ixx> ".div_s"
    // <ixx> ".div_u"
    // <ixx> ".rem_s"
    // <ixx> ".rem_u"
    // <ixx> ".or"
    // <ixx> ".xor"
    // <ixx> ".shl"
    // <ixx> ".shr_s"
    // <ixx> ".shr_u"
    // <ixx> ".rotl"
    // <ixx> ".rotr"
    // <fxx> ".add"
    // <fxx> ".sub"
    // <fxx> ".mul"
    // <fxx> ".div"
    // <fxx> ".min"
    // <fxx> ".max"
    // <fxx> ".copysign"
}

#[derive(Clone, Debug)]
pub struct Binding<'i> {
    pub name: Option<Name<'i>>,
    pub type_: ValueType,
}

impl<'i> From<(Name<'i>, ValueType)> for Binding<'i> {
    fn from(v: (Name<'i>, ValueType)) -> Self {
        Binding {
            name: Some(v.0),
            type_: v.1,
        }
    }
}

impl From<ValueType> for Binding<'static> {
    fn from(v: ValueType) -> Self {
        Binding {
            name: None,
            type_: v,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Branch<'i> {
    pub label: Option<Name<'i>>,
    pub body: Vec<Expr<'i>>,
}

impl<'i> From<Expr<'i>> for Branch<'i> {
    fn from(v: Expr<'i>) -> Self {
        Branch {
            label: None,
            body: vec![v],
        }
    }
}

impl<'i> From<Vec<Expr<'i>>> for Branch<'i> {
    fn from(v: Vec<Expr<'i>>) -> Self {
        Branch {
            label: None,
            body: v,
        }
    }
}

impl<'i> From<(Option<Name<'i>>, Vec<Expr<'i>>)> for Branch<'i> {
    fn from((label, body): (Option<Name<'i>>, Vec<Expr<'i>>)) -> Self {
        Branch {
            label: label,
            body: body,
        }
    }
}

impl<'i> From<(Option<Name<'i>>, Expr<'i>)> for Branch<'i> {
    fn from((label, body): (Option<Name<'i>>, Expr<'i>)) -> Self {
        Branch {
            label: label,
            body: vec![body],
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Compare {
    F32Eq,
    F32Ne,
    F64Eq,
    F64Ne,
    I32Eq,
    I32Ne,
    I64Eq,
    I64Ne,
    // ...
}

#[derive(Copy, Clone, Debug)]
pub enum Const {
    I32,
    I64,
    F32,
    F64,
}

#[derive(Clone, Debug)]
pub enum Export<'i> {
    Memory(Cow<'i, str>, Memory<'i>),
    Var(Cow<'i, str>, Var<'i>),
}

#[derive(Clone, Debug)]
pub enum Expr<'i> {
    Binary(Binary, Box<Expr<'i>>, Box<Expr<'i>>),
    Block(Branch<'i>),
    Br(Var<'i>, Option<Box<Expr<'i>>>),
    Call(Var<'i>, Vec<Expr<'i>>),
    CallImport(Var<'i>, Vec<Expr<'i>>),
    Compare(Compare, Box<Expr<'i>>, Box<Expr<'i>>),
    Const(Const, Value),
    If(Box<Expr<'i>>, Branch<'i>, Option<Branch<'i>>),
    IfElseNoBlock(Box<Expr<'i>>, Box<Expr<'i>>, Box<Expr<'i>>),
    GetLocal(Var<'i>),
    Label(Branch<'i>),
    Load(Load, Option<Offset>, Option<Align>, Box<Expr<'i>>),
    Loop(LoopLabels<'i>, Vec<Expr<'i>>), 
    Return(Option<Box<Expr<'i>>>),
    SetLocal(Var<'i>, Box<Expr<'i>>),
    Store(Store, Option<Offset>, Option<Align>, Box<Expr<'i>>, Box<Expr<'i>>),
    Unary(Unary, Box<Expr<'i>>),
}

#[derive(Clone, Debug)]
pub struct Func<'i> {
    pub name: Option<Name<'i>>,
    pub params: Vec<Binding<'i>>,
    pub result: Option<ValueType>,
    pub locals: Vec<Binding<'i>>,
    pub exprs: Vec<Expr<'i>>,
}

#[derive(Clone, Debug)]
pub enum FuncField<'i> {
    AnonLocals(Vec<ValueType>),
    AnonParams(Vec<ValueType>),
    Local(Name<'i>, ValueType),
    Param(Name<'i>, ValueType),
    Result(ValueType),
}

#[derive(Clone, Debug)]
pub struct Import<'i> {
    pub name: Option<Name<'i>>,
    pub import_module: Cow<'i, str>,
    pub import_name: Cow<'i, str>,
    pub result: Option<ValueType>,
    pub params: Vec<ValueType>,
}

#[derive(Copy, Clone, Debug)]
pub enum Load {
    F32,
    F64,
    I32,
    I32FromI8,
    I32FromU8,
    I32FromI16,
    I32FromU16,
    I64,
    I64FromI8,
    I64FromU8,
    I64FromI16,
    I64FromU16,
    I64FromI32,
    I64FromU32,
}

#[derive(Clone, Debug)]
pub enum LoopLabels<'i> {
    None,
    Cont(Name<'i>),
    ExitCont(Name<'i>, Name<'i>),
}

impl<'i> From<Option<Name<'i>>> for LoopLabels<'i> {
    fn from(v: Option<Name<'i>>) -> Self {
        match v {
            None => LoopLabels::None,
            Some(v) => LoopLabels::Cont(v),
        }
    }
}

impl<'i> From<(Name<'i>, Name<'i>)> for LoopLabels<'i> {
    fn from(v: (Name<'i>, Name<'i>)) -> Self {
        LoopLabels::ExitCont(v.0, v.1)
    }
}

#[derive(Clone, Debug)]
pub struct Memory<'i> {
    pub min: Size,
    pub max: Option<Size>,
    pub segments: Vec<Segment<'i>>,
}

#[derive(Clone, Debug)]
pub struct Module<'i> {
    pub entries: Vec<ModuleEntry<'i>>,
}

#[derive(Clone, Debug)]
pub enum ModuleEntry<'i> {
    Export(Export<'i>),
    Func(Func<'i>),
    Import(Import<'i>),
    Memory(Memory<'i>),
}

#[derive(Clone, Debug)]
pub struct Name<'i>(Cow<'i, str>);

impl<'i> Deref for Name<'i> {
    type Target = Cow<'i, str>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'i> fmt::Display for Name<'i> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl<'i> From<&'i str> for Name<'i> {
    fn from(v: &'i str) -> Self {
        Name(v.into())
    }
}

impl<'i> From<String> for Name<'i> {
    fn from(v: String) -> Self {
        Name(v.into())
    }
}

impl<'i> From<Cow<'i, str>> for Name<'i> {
    fn from(v: Cow<'i, str>) -> Self {
        Name(v)
    }
}

impl<'i> Into<Cow<'i, str>> for Name<'i> {
    fn into(self) -> Cow<'i, str> {
        self.0
    }
}

#[derive(Clone, Debug)]
pub struct Script<'i> {
    pub entries: Vec<ScriptEntry<'i>>,
}

#[derive(Clone, Debug)]
pub enum ScriptEntry<'i> {
    AssertInvalid(AssertInvalid<'i>),
    AssertReturn(AssertReturn<'i>),
    AssertTrap(AssertTrap<'i>),
    Module(Module<'i>),
}

#[derive(Clone, Debug)]
pub struct Segment<'i> {
    pub offset: Size,
    pub data: Cow<'i, [u8]>,
}

#[derive(Copy, Clone, Debug)]
pub enum Store {
    F32,
    F64,
    I32,
    I32AsB8,
    I32AsB16,
    I64,
    I64AsB8,
    I64AsB16,
    I64AsB32,
}

#[derive(Copy, Clone, Debug)]
pub enum Unary {
    F64FromI32,
    F64FromI64Bits,
}

#[derive(Copy, Clone, Debug)]
pub enum Value {
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
}

impl Value {
    pub fn coerce_const(self, const_: Const) -> Option<Value> {
        match (self, const_) {
            (Value::I64(v), Const::I32) => Some(Value::I32(v as i32)),
            (Value::I64(v), Const::I64) => Some(Value::I64(v)),
            (Value::I64(v), Const::F32) => Some(Value::F32(v as f32)),
            (Value::I64(v), Const::F64) => Some(Value::F64(v as f64)),

            (Value::F64(v), Const::F32) => Some(Value::F32(v as f32)),
            (Value::F64(v), Const::F64) => Some(Value::F64(v)),

            _ => None
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum ValueType {
    I32,
    I64,
    F32,
    F64,
}

#[derive(Clone, Debug)]
pub enum Var<'i> {
    Id(Id),
    Name(Name<'i>),
}
