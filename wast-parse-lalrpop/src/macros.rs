macro_rules! err_into {
    ($e:expr) => {
        match $e {
            ::std::result::Result::Ok(v) => ::std::result::Result::Ok(v),
            ::std::result::Result::Err(err) => ::std::result::Result::Err(::std::convert::From::from(err)),
        }
    };
}
