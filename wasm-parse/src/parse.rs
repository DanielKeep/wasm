use model::*;
use nom::{self, ErrorKind, Needed};
use nom::IResult::{Done};

pub type WResult<I, O> = nom::IResult<I, O, WasmErrorKind>;

pub enum WasmErrorKind {
    VarIntRange,
}

pub fn module_(bs: &[u8]) -> WResult<&[u8], Module> {
    module(bs)
}

named! {
    module <&[u8], Module, WasmErrorKind>,
    chain!(
        fix_error!(WasmErrorKind, tag!("\u{0}asm"))
        ~ version: uint32
        ~ sections: many0!(section)
        ,
        || Module {
            version: version,
            sections: sections,
        }
    )
}

named! {
    section <&[u8], Section, WasmErrorKind>,
    chain!(
        size: varuint32
        ~ id_len: varuint32
        ~ id_str: fix_error!(WasmErrorKind, take_str!(id_len))
        ~ data: fix_error!(WasmErrorKind, take!(size - id_len))
        ~ section: call!(section_dispatch, id_str, data)
        ,
        || section
    )
}

fn section_dispatch<'i>(
    bs: &'i[u8],
    id: &'i str,
    data: &'i [u8]
) -> WResult<&'i [u8], Section<'i>> {
    match id {
        _ => Done(bs, Section::Unknown {
            id: id.into(),
            data: data.into()
        })
    }
}

named! {
    uint32 <&[u8], u32, WasmErrorKind>,
    fix_error!(WasmErrorKind, u32!(false))
}

fn varuint32(bs: &[u8]) -> WResult<&[u8], u32> {
    macro_rules! step {
        ($acc:expr, $bs:expr, $shift:expr) => {
            match $bs.first() {
                Some(&b) if b < 0x80 => return Done(&$bs[1..], $acc | (b as u32) << $shift),
                Some(&b) => ($acc | ((b & 0x7f) as u32) << $shift, &$bs[1..]),
                None => return nom::IResult::Incomplete(Needed::Unknown),
            }
        };
    }

    let (acc, bs) = step!(0, bs, 0);
    let (acc, bs) = step!(acc, bs, 7);
    let (acc, bs) = step!(acc, bs, 14);
    let (acc, bs) = step!(acc, bs, 21);

    match bs.first() {
        Some(&b) if b < 0b0001_0000 => Done(&bs[1..], acc | (b as u32) << 28),
        Some(&_) => nom::IResult::Error(nom::Err::Position(
                ErrorKind::Custom(WasmErrorKind::VarIntRange), bs)),
        None => nom::IResult::Incomplete(Needed::Size(1)),
    }
}
