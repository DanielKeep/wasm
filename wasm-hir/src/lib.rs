#[macro_use] extern crate custom_derive;
#[macro_use] extern crate newtype_derive;
extern crate conv;

use conv::prelude::*;
use conv::errors::Unrepresentable;

macro_rules! table_types {
    (@as_items $($i:item)*) => { $($i)* };

    (<$lt:tt> $entry:ty, $id:ident) => {
        table_types! {
            @as_items

            impl<$lt> TableEntry for $entry {
                type Id = $id;
            }

            impl TableId for $id {
                fn from_usize(v: usize) -> Self {
                    $id(v.value_into().unwrap())
                }

                fn into_usize(self) -> usize {
                    (self.0).value_into().unwrap()
                }
            }
        }
    };

    ($id:ident) => {
        table_types! {
            @as_items

            impl TableId for $id {
                fn from_usize(v: usize) -> Self {
                    $id(v.value_into().unwrap())
                }

                fn into_usize(self) -> usize {
                    (self.0).value_into().unwrap()
                }
            }
        }
    };
}

use std::borrow::Cow;
use std::fmt;
use std::ops::Deref;

pub trait TableEntry {
    type Id: TableId;
}

pub trait TableId: Sized + Copy {
    fn from_usize(usize) -> Self;
    fn into_usize(self) -> usize;
}

pub type Align = u16;
pub type BranchDepth = u16;
pub type Offset = i64;
pub type RawId = u64;

#[derive(Copy, Clone, Debug)]
pub enum Arch {
    Wasm32,
}

impl Arch {
    pub fn address_type(self) -> Type {
        self.address_value_type().into_type()
    }

    pub fn address_value_type(self) -> ValueType {
        match self {
            Arch::Wasm32 => ValueType::I32,
        }
    }
}

#[derive(Clone, Debug)]
pub enum Assert<'i> {
    Return(Cow<'i, str>, Vec<Value>, Option<Value>),
    Trap(Cow<'i, str>, Vec<Value>, Cow<'i, str>),
}

#[derive(Copy, Clone, Debug)]
pub enum Binary {
    I32Add,
    I32And,
    I32Mul,
    I32Sub,

    I64Add,
    I64And,
    I64Mul,
    I64Sub,
}

impl Binary {
    pub fn types(self) -> (Type, Type, Type) {
        use self::Binary::*;
        const I32: Type = Type::Value(ValueType::I32);
        const I64: Type = Type::Value(ValueType::I64);
        match self {
            I32Add
            | I32Mul
            | I32Sub
            => (I32, I32, I32),
            I32And
            => (I32, I32, I32),
            I64Add
            | I64Mul
            | I64Sub
            => (I64, I64, I64),
            I64And
            => (I32, I64, I64),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Binding<'i> {
    pub name: Option<Name<'i>>,
    pub type_: ValueType,
}

// #[derive(Copy, Clone, Debug)]
// pub enum Branch {
//     Cont(BranchDepth),
//     Exit(BranchDepth, Box<Expr>),
// }

// impl Branch {
//     pub fn is_exit(self) -> bool {
//         match self {
//             Branch::Cont(_) => false,
//             Branch::Exit(_) => true,
//         }
//     }
// }

#[derive(Copy, Clone, Debug)]
pub enum Compare {
    F32Eq,
    F32Ne,
    F64Eq,
    F64Ne,
    I32Eq,
    I32Ne,
    I64Eq,
    I64Ne,
}

impl Compare {
    pub fn types(self) -> (Type, Type) {
        use self::Compare::*;
        const F32: Type = Type::Value(ValueType::F32);
        const F64: Type = Type::Value(ValueType::F64);
        const I32: Type = Type::Value(ValueType::I32);
        const I64: Type = Type::Value(ValueType::I64);
        match self {
            F32Eq => (F32, F32),
            F32Ne => (F32, F32),
            F64Eq => (F64, F64),
            F64Ne => (F64, F64),
            I32Eq => (I32, I32),
            I32Ne => (I32, I32),
            I64Eq => (I64, I64),
            I64Ne => (I64, I64),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Export<'i> {
    pub name: Cow<'i, str>,
    pub func: FuncId,
}

#[derive(Clone, Debug)]
pub struct Expr {
    pub type_: Type,
    pub node: ExprNode,
}

#[derive(Clone, Debug)]
pub enum ExprNode {
    Binary(Binary, Box<Expr>, Box<Expr>),
    Block(Vec<Expr>),
    Br(BranchDepth, Option<Box<Expr>>),
    Call(FuncId, Vec<Expr>),
    CallImport(ImportId, Vec<Expr>),
    Compare(Compare, Box<Expr>, Box<Expr>),
    Const(Value),
    If(Box<Expr>, Box<Expr>, Option<Box<Expr>>),
    GetLocal(LocalId),
    Load(Load, Offset, Align, Box<Expr>),
    Loop(Vec<Expr>),
    Return(Option<Box<Expr>>),
    SetLocal(LocalId, Box<Expr>),
    Store(Store, Offset, Align, Box<Expr>, Box<Expr>),
    Unary(Unary, Box<Expr>),
}

table_types! { <'i> Func<'i>, FuncId }

#[derive(Clone, Debug)]
pub struct Func<'i> {
    pub name: Option<Name<'i>>,
    pub result: Type,
    pub params: Vec<Binding<'i>>,
    pub locals: Vec<Binding<'i>>,
    pub body: Vec<Expr>,
}

custom_derive! {
    #[derive(Copy, Clone, Debug)]
    #[derive(NewtypeFrom)]
    pub struct FuncId(RawId);
}

table_types! { <'i> Import<'i>, ImportId }

#[derive(Clone, Debug)]
pub struct Import<'i> {
    pub name: Option<Name<'i>>,
    pub module: Cow<'i, str>,
    pub sig: ImportSig<'i>,
}

custom_derive! {
    #[derive(Copy, Clone, Debug)]
    #[derive(NewtypeFrom)]
    pub struct ImportId(RawId);
}

#[derive(Clone, Debug, Hash)]
pub struct ImportSig<'i> {
    pub name: Cow<'i, str>,
    pub result: Type,
    pub params: Vec<ValueType>,
}

table_types! { LocalId }

custom_derive! {
    #[derive(Copy, Clone, Debug)]
    #[derive(NewtypeFrom)]
    pub struct LocalId(RawId);
}

#[derive(Copy, Clone, Debug)]
pub enum Load {
    F32,
    F64,
    I32,
    I32FromI8,
    I32FromU8,
    I32FromI16,
    I32FromU16,
    I64,
    I64FromI8,
    I64FromU8,
    I64FromI16,
    I64FromU16,
    I64FromI32,
    I64FromU32,
}

impl Load {
    pub fn type_(self) -> Type {
        const F32: Type = Type::Value(ValueType::F32);
        const F64: Type = Type::Value(ValueType::F64);
        const I32: Type = Type::Value(ValueType::I32);
        const I64: Type = Type::Value(ValueType::I64);
        match self {
            Load::F32 => F32,
            Load::F64 => F64,
            Load::I32 => I32,
            Load::I32FromI8 => I32,
            Load::I32FromU8 => I32,
            Load::I32FromI16 => I32,
            Load::I32FromU16 => I32,
            Load::I64 => I64,
            Load::I64FromI8 => I64,
            Load::I64FromU8 => I64,
            Load::I64FromI16 => I64,
            Load::I64FromU16 => I64,
            Load::I64FromI32 => I64,
            Load::I64FromU32 => I64,
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Memory<'i> {
    pub min: u64,
    pub max: u64,
    pub segments: Vec<Segment<'i>>,
}

#[derive(Clone, Debug)]
pub struct Module<'i> {
    pub exports: Vec<Export<'i>>,
    pub funcs: Vec<Func<'i>>,
    pub imports: Vec<Import<'i>>,
    pub memory: Memory<'i>,
}

#[derive(Clone, Debug)]
pub struct Name<'i>(Cow<'i, str>);

impl<'i> Deref for Name<'i> {
    type Target = Cow<'i, str>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'i> fmt::Display for Name<'i> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl<'i> From<&'i str> for Name<'i> {
    fn from(v: &'i str) -> Self {
        Name(v.into())
    }
}

impl<'i> From<String> for Name<'i> {
    fn from(v: String) -> Self {
        Name(v.into())
    }
}

impl<'i> From<Cow<'i, str>> for Name<'i> {
    fn from(v: Cow<'i, str>) -> Self {
        Name(v)
    }
}

impl<'i> Into<Cow<'i, str>> for Name<'i> {
    fn into(self) -> Cow<'i, str> {
        self.0
    }
}

#[derive(Clone, Debug)]
pub struct Script<'i> {
    pub asserts: Vec<Assert<'i>>,
    pub modules: Vec<Module<'i>>,
    pub export_module: usize,
}

#[derive(Clone, Debug)]
pub struct Segment<'i> {
    pub offset: u64,
    pub data: Cow<'i, [u8]>,
}

#[derive(Copy, Clone, Debug)]
pub enum Store {
    F32,
    F64,
    I32,
    I32AsB8,
    I32AsB16,
    I64,
    I64AsB8,
    I64AsB16,
    I64AsB32,
}

impl Store {
    pub fn type_(self) -> Type {
        const F32: Type = Type::Value(ValueType::F32);
        const F64: Type = Type::Value(ValueType::F64);
        const I32: Type = Type::Value(ValueType::I32);
        const I64: Type = Type::Value(ValueType::I64);
        match self {
            Store::F32 => F32,
            Store::F64 => F64,
            Store::I32 => I32,
            Store::I32AsB8 => I32,
            Store::I32AsB16 => I32,
            Store::I64 => I64,
            Store::I64AsB8 => I64,
            Store::I64AsB16 => I64,
            Store::I64AsB32 => I64,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum Type {
    Void,
    Divergent,
    Value(ValueType),
}

impl Type {
    pub fn unify_with(self, other: Type) -> Option<Type> {
        use self::Type::*;
        match (self, other) {
            (Divergent, ty) | (ty, Divergent) => Some(ty),
            (Void, Void) => Some(Void),
            (Value(lhs), Value(rhs)) if lhs == rhs => Some(self),
            _ => None,
        }
    }

    pub fn unifies_with(self, other: Type) -> bool {
        self.unify_with(other).is_some()
    }
}

impl From<ValueType> for Type {
    fn from(v: ValueType) -> Self {
        Type::Value(v)
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Unary {
    F64FromI32,
    F64FromI64Bits,
}

impl Unary {
    pub fn types(self) -> (Type, Type) {
        use self::Unary::*;
        const F64: Type = Type::Value(ValueType::F64);
        const I32: Type = Type::Value(ValueType::I32);
        const I64: Type = Type::Value(ValueType::I64);
        match self {
            F64FromI32 => (F64, I32),
            F64FromI64Bits => (F64, I64),
        }
    }
}

#[derive(Copy, Clone, PartialEq, PartialOrd, Debug)]
pub enum Value {
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
}

impl Value {
    pub fn as_i32(self) -> i32 {
        match self {
            Value::I32(v) => v,
            _ => panic!("expected I32(_), got {:?}", self)
        }
    }

    pub fn as_i64(self) -> i64 {
        match self {
            Value::I64(v) => v,
            _ => panic!("expected I64(_), got {:?}", self)
        }
    }

    pub fn as_f32(self) -> f32 {
        match self {
            Value::F32(v) => v,
            _ => panic!("expected F32(_), got {:?}", self)
        }
    }

    pub fn as_f64(self) -> f64 {
        match self {
            Value::F64(v) => v,
            _ => panic!("expected F64(_), got {:?}", self)
        }
    }

    pub fn coerce_to(self, ty: ValueType) -> Value {
        fn coerce_f64(v: f64, ty: ValueType) -> Value {
            match ty {
                ValueType::I32 => Value::I32(v as i32),
                ValueType::I64 => Value::I64(v as i64),
                ValueType::F32 => Value::F32(v as f32),
                ValueType::F64 => Value::F64(v),
            }
        }

        fn coerce_i64(v: i64, ty: ValueType) -> Value {
            match ty {
                ValueType::I32 => Value::I32(v as i32),
                ValueType::I64 => Value::I64(v),
                ValueType::F32 => Value::F32(v as f32),
                ValueType::F64 => Value::F64(v as f64),
            }
        }

        match self {
            Value::I32(v) => coerce_i64(v as i64, ty),
            Value::I64(v) => coerce_i64(v, ty),
            Value::F32(v) => coerce_f64(v as f64, ty),
            Value::F64(v) => coerce_f64(v, ty),
        }
    }

    pub fn type_(self) -> Type {
        use self::Value::*;
        match self {
            I32(_) => Type::Value(ValueType::I32),
            I64(_) => Type::Value(ValueType::I64),
            F32(_) => Type::Value(ValueType::F32),
            F64(_) => Type::Value(ValueType::F64),
        }
    }
}

impl From<i32> for Value { fn from(v: i32) -> Self { Value::I32(v) } }
impl From<i64> for Value { fn from(v: i64) -> Self { Value::I64(v) } }
impl From<f32> for Value { fn from(v: f32) -> Self { Value::F32(v) } }
impl From<f64> for Value { fn from(v: f64) -> Self { Value::F64(v) } }

impl From<bool> for Value { fn from(v: bool) -> Self { Value::I32(v as i32) } }

impl ValueFrom<Value> for i32 {
    type Err = Unrepresentable<Value>;
    fn value_from(src: Value) -> Result<Self, Self::Err> {
        match src {
            Value::I32(v) => Ok(v),
            other => Err(Unrepresentable(other)),
        }
    }
}

impl ValueFrom<Value> for i64 {
    type Err = Unrepresentable<Value>;
    fn value_from(src: Value) -> Result<Self, Self::Err> {
        match src {
            Value::I64(v) => Ok(v),
            other => Err(Unrepresentable(other)),
        }
    }
}

impl ValueFrom<Value> for f32 {
    type Err = Unrepresentable<Value>;
    fn value_from(src: Value) -> Result<Self, Self::Err> {
        match src {
            Value::F32(v) => Ok(v),
            other => Err(Unrepresentable(other)),
        }
    }
}

impl ValueFrom<Value> for f64 {
    type Err = Unrepresentable<Value>;
    fn value_from(src: Value) -> Result<Self, Self::Err> {
        match src {
            Value::F64(v) => Ok(v),
            other => Err(Unrepresentable(other)),
        }
    }
}

impl ValueFrom<Value> for bool {
    type Err = Unrepresentable<Value>;
    fn value_from(src: Value) -> Result<Self, Self::Err> {
        match src {
            Value::I32(v) => Ok(v != 0),
            other => Err(Unrepresentable(other)),
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum ValueType {
    I32,
    I64,
    F32,
    F64,
}

impl ValueType {
    pub fn initializer(self) -> Value {
        match self {
            ValueType::I32 => Value::I32(0),
            ValueType::I64 => Value::I64(0),
            ValueType::F32 => Value::F32(0.0),
            ValueType::F64 => Value::F64(0.0),
        }
    }

    pub fn into_type(self) -> Type {
        Type::Value(self)
    }
}

pub trait ValueTypeEquiv {
    fn value_type() -> ValueType;
}

impl ValueTypeEquiv for i32 { fn value_type() -> ValueType { ValueType::I32 } }
impl ValueTypeEquiv for i64 { fn value_type() -> ValueType { ValueType::I64 } }
impl ValueTypeEquiv for f32 { fn value_type() -> ValueType { ValueType::F32 } }
impl ValueTypeEquiv for f64 { fn value_type() -> ValueType { ValueType::F64 } }
impl ValueTypeEquiv for bool { fn value_type() -> ValueType { ValueType::I32 } }
